--==============================================================
/*
SCRIPT:
    a03_sfx_selected.sql

PURPOSE:
    - Uploaded selected audience from S3 to redshift

INPUTS:

OUTPUTS:

DEVELOPMENT:
    Version 1.0 - Joseph Nguyen | 01Sep2020
    ---
*/
--==============================================================
-------------------------------------------------


drop table if exists loyalty_modeling.cna3577_sfx_selected;
create table loyalty_modeling.cna3577_sfx_selected
(
    campaign_code          varchar(20)
    ,campaign_start_date    date
    ,ref_dt                 date
    ,crn                    varchar(20)
    ,audience_type_final    varchar(40)
    ,audience_type          varchar(10)
    ,target_segment         varchar(40)
    ,score                  numeric(10,7)
    ,macro_segment_curr     varchar(20)
    ,affluence              varchar(20)
    ,lifestage              varchar(50)
    ,gender                 char(1)
    ,age                    int
)
distkey (crn)
;
grant select on loyalty_modeling.cna3577_sfx_selected to public;

copy loyalty_modeling.cna3577_sfx_selected
from 's3://data-preprod-redshift-exports/Joe/cna3577_sfx_selected.csv'
CREDENTIALS 'aws_access_key_id=AKIA4KLQCVHRE53KRCQK;aws_secret_access_key=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' 
CSV IGNOREHEADER 1;
-- select * from stl_load_errors;
--^ Warning [01000 / 0]: Load into table 'mel_ds_load_20200720' completed, 10063323 record(s) loaded successfully.
select count(*), count(distinct crn) from loyalty_modeling.cna3577_sfx_selected; -- [10063323	10063323]


select
    audience_type_final
    ,count(*) as vol
    ,avg(score) as score
from loyalty_modeling.cna3577_sfx_selected
group by 1
order by 1;
