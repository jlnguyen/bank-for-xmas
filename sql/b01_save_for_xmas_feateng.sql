--==============================================================
/*
SCRIPT:
    b01_save_for_xmas_feateng.sql

PURPOSE:
    - Feature engineering exploration
    - Identify trends in reponse (is_save_for_xmas) against features
        - Graph in google sheets
    - Inform feature selection for modelling

INPUTS:

OUTPUTS:

DEVELOPMENT:
    Version 1.0 - Joseph Nguyen | 07Jul2020
    ---
*/
--==============================================================
-------------------------------------------------


-------------------------------------------------
-- CVM Segment
-------------------------------------------------
select
    bse.is_save_for_xmas
    ,macro_segment_curr
    ,count(*)::float / y_cnt
    ,sum(smpl_wgt)::float / y_cnt_wgt
    ,count(*) as cnt
    ,y_cnt
    ,sum(smpl_wgt) as cnt_wgt
    ,y_cnt_wgt
from loyalty_modeling.sfx_model_data as bse
join (
    select
        is_save_for_xmas
        ,count(*) as y_cnt
        ,sum(smpl_wgt) as y_cnt_wgt
    from loyalty_modeling.sfx_model_data
    group by 1
) as tot
on tot.is_save_for_xmas = bse.is_save_for_xmas
group by 1,2, y_cnt, y_cnt_wgt
order by 1,2;

-------------------------------------------------
-- Affluence
-------------------------------------------------
select
    bse.is_save_for_xmas
    ,affluence
    ,count(*)::float / y_cnt
    ,count(*) as cnt
from loyalty_modeling.sfx_model_data as bse
join (
    select is_save_for_xmas, count(*) as y_cnt
    from loyalty_modeling.sfx_model_data
    group by 1
) as tot
on tot.is_save_for_xmas = bse.is_save_for_xmas
group by 1,2, y_cnt
order by 1,2;

-------------------------------------------------
-- Lifestage
-------------------------------------------------
select
    bse.is_save_for_xmas
    ,lifestage
    ,count(*)::float / y_cnt
    ,count(*) as cnt
from loyalty_modeling.sfx_model_data as bse
join (
    select is_save_for_xmas, count(*) as y_cnt
    from loyalty_modeling.sfx_model_data
    group by 1
) as tot
on tot.is_save_for_xmas = bse.is_save_for_xmas
group by 1,2, y_cnt
order by 1,2;

-------------------------------------------------
-- Continuous variables
-------------------------------------------------
select
    is_save_for_xmas
    ,sum(age * smpl_wgt) / sum(smpl_wgt) as age_avg_wgt
    ,sum(tenure * smpl_wgt) / sum(smpl_wgt) as tenure_avg_wgt
    ,sum(spend_8wk * smpl_wgt) / sum(smpl_wgt) as spend_8wk_avg_wgt
    ,sum(stretch_perc_raw * smpl_wgt) / sum(smpl_wgt) as stretch_perc_raw_avg_wgt
    ,count(*) as cnt
from loyalty_modeling.sfx_model_data
where spend_8wk > 1
group by 1
order by 1
;

-------------------------------------------------
-- Age
-------------------------------------------------
select
    bse.is_save_for_xmas
    ,age_band
    ,count(*)::float / y_cnt
    ,count(*) as cnt
from loyalty_modeling.sfx_model_data as bse
join (
    select is_save_for_xmas, count(*) as y_cnt
    from loyalty_modeling.sfx_model_data
    group by 1
) as tot
on tot.is_save_for_xmas = bse.is_save_for_xmas
group by 1,2, y_cnt
order by 1,2;

-------------------------------------------------
-- Tenure
-------------------------------------------------
select
    bse.is_save_for_xmas
    ,tenure_band
    ,count(*)::float / y_cnt
    ,count(*) as cnt
from loyalty_modeling.sfx_model_data as bse
join (
    select is_save_for_xmas, count(*) as y_cnt
    from loyalty_modeling.sfx_model_data
    group by 1
) as tot
on tot.is_save_for_xmas = bse.is_save_for_xmas
group by 1,2, y_cnt
order by 1,2;

-------------------------------------------------
-- Gender
-------------------------------------------------
select
    bse.is_save_for_xmas
    ,gender
    ,count(*)::float / y_cnt
    ,count(*) as cnt
from loyalty_modeling.sfx_model_data as bse
join (
    select is_save_for_xmas, count(*) as y_cnt
    from loyalty_modeling.sfx_model_data
    group by 1
) as tot
on tot.is_save_for_xmas = bse.is_save_for_xmas
where spend_8wk > 1
group by 1,2, y_cnt
order by 1,2;


-------------------------------------------------
-- Age
-------------------------------------------------
select
    age_band
    ,count(*) as cnt
from loyalty_modeling.sfx_model_data as bse
group by 1
order by 1;

-------------------------------------------------
-- Segment vs Affluence
-------------------------------------------------
select
    bse.is_save_for_xmas
    ,case when macro_segment_curr = '' then NULL
        else macro_segment_curr
    end as macro_segment_curr
    ,case when affluence = '' then NULL
        else affluence
    end as affluence
    ,count(*)::float / y_cnt
    ,count(*) as cnt
from loyalty_modeling.sfx_model_data as bse
join (
    select is_save_for_xmas, count(*) as y_cnt
    from loyalty_modeling.sfx_model_data
    group by 1
) as tot
on tot.is_save_for_xmas = bse.is_save_for_xmas
group by 1,2,3, y_cnt
order by 1,2,3;


--==============================================================
/*
    QA
*/
--==============================================================
select pref_option_cnt, count(*) as crn_cnt
from
(
    select crn, count(distinct pref_option) as pref_option_cnt
    from loyalty_modeling.sfx_crn_preference_all
    group by 1
)z
group by 1
order by 1;

-- ref_dt == Sunday
select smpl_wgt, to_char(ref_dt, 'Day'), count(*)
from loyalty_modeling.sfx_model_data
group by 1,2
order by 1,2;

-------------------------------------------------
-- Explore known customers
-------------------------------------------------
select cn.*, cpa.*
from loyalty.sfl_preferences as cpa
right join
    loyalty_modeling.jn_cust_name as cn
on cn.crn = cpa.crn;

select cn.*, cpa.*
from loyalty.customer_value_model as cpa
right join
    loyalty_modeling.jn_cust_name as cn
on cn.crn = cpa.crn
where cpa.pw_end_date = '2020-06-30'::date;

select cn.*, cpa.*
from loyalty_modeling.sfx_model_data as cpa
right join
    loyalty_modeling.jn_cust_name as cn
on cn.crn = cpa.crn;