--==============================================================
/*
SCRIPT:
    z01_explr_bank_for_xmas.sql

PURPOSE:
    - Bank for xmas
    - Two options for claiming reward points:
        1. Auto-redeem $10 from 2000 pts
        2. Save up for December (xmas)
    - Analytics have shown that customers who claim option 2 spend
    more than option 1 during xmas
    - We identify customers who switch from option 1 to 2 to build a
    model to target customers with high propensity to switch

INPUTS:

OUTPUTS:

DEVELOPMENT:
    Version 1.0 - Joseph Nguyen | 01Jul2020
    ---
*/
--==============================================================
-------------------------------------------------

/*  Check known crns */
select
    cn.*
    ,bse.*
from loyalty.dc_sfl_optout_members as bse
-- from loyalty.sfl_preferences_hist as bse
right join
    loyalty_modeling.jn_cust_name as cn
on cn.crn = bse.crn;

select *
from loyalty.sfl_preferences_hist as bse
where lylty_card_nbr = '-5023352380067849855'
selec

select count(*), count(distinct crn)
from
loyalty.dc_sfl_optout_members
loyalty.sfl_preferences_hist

/* Join hash card number table */
select cnt, count(*) as crn_card_cnt
from
(
    select
        sfl.crn
        ,hsh.lylty_card_nbr
        -- ,hsh.row_status_ind
        ,count(*) as cnt
    from loyalty.sfl_preferences as sfl
    left join
        loyalty.lylty_card_hash as hsh
    on hsh.lylty_card_nbr_hash = sfl.lylty_card_nbr
    group by 1,2
)z
group by 1
;

/* Multiple card status: card status 1 has row_status_ind = {I,U} */
select
    lylty_card_status
    ,lylty_card_rgstr_flag
    ,sfl.row_status_ind
    ,count(*) as cnt
from loyalty.sfl_preferences as sfl
left join
    loyalty.lylty_card_detail as lcd
on lcd.crn = sfl.crn
    and lcd.lylty_card_nbr = sfl.lylty_card_nbr
group by 1,2,3
order by 1,2,3;

/* 3 crns with two rows */
select row_cnt, count(*) as crn_cnt
from
(
    select
        sfl.crn
        ,count(*) as row_cnt
    from loyalty.sfl_preferences as sfl
    left join
        loyalty.lylty_card_detail as lcd
    on lcd.crn = sfl.crn
        and lcd.lylty_card_nbr = sfl.lylty_card_nbr
    where lylty_card_status = 1
        and lylty_card_rgstr_flag = 'Y'
    group by 1
)z
group by 1
order by 1;

/* View the 3 crns data */
select *
from loyalty.sfl_preferences as sfl
join
(
    select
        sfl.crn
        ,count(*) as row_cnt
    from loyalty.sfl_preferences as sfl
    left join
        loyalty.lylty_card_detail as lcd
    on lcd.crn = sfl.crn
        and lcd.lylty_card_nbr = sfl.lylty_card_nbr
    where lylty_card_status = 1
        and lylty_card_rgstr_flag = 'Y'
    group by 1
) as agg
on agg.crn = sfl.crn
where agg.row_cnt > 1
order by sfl.crn;

/* crns with > 1 pref_dt always have > 1 card */
select
    card_nbr_cnt
    ,pref_dt_cnt
    ,count(*) as crn_cnt
from
(
    select crn
        ,count(distinct lylty_card_nbr) as card_nbr_cnt
        ,count(distinct trunc(sflpreferenceauditdatetime)) as pref_dt_cnt
    from loyalty.sfl_preferences as sfl
    group by 1
)z
group by 1,2
order by 1,2;

select top 188
    trunc(sflpreferenceauditdatetime) as pref_dt
    ,*
from loyalty.sfl_preferences as bse
join
(
    select crn
        ,count(distinct trunc(sflpreferenceauditdatetime)) as pref_dt_cnt
    from loyalty.sfl_preferences as sfl
    group by 1
) as agg
on agg.crn = bse.crn
where agg.pref_dt_cnt > 1
order by bse.crn, bse.lylty_card_nbr, pref_dt;

/* Identify switchers (option 1 to 2 ): Chrislyn */
with sfl as (
    select
    sfl.crn
        , lylty_card_nbr
        , case when sfloptin='true' and sfloptionid<>2 then 'SFL'
            when sfloptin='true' and sfloptionid=2 then 'QFF'
        end as pref
        , trunc(sflpreferenceauditdatetime) sflpreferenceauditdatetime
        , ROW_NUMBER () OVER (
            PARTITION BY sfl.crn
            ORDER BY pref, sflpreferenceauditdatetime desc
        ) AS most_recent
    from loyalty.sfl_preferences as sfl
    where 1=1 
        and sfloptin='true'
        and sfloptionid in (1,2)
)
select
    pref, count(distinct sfl.crn)
from sfl
inner join loyalty.lylty_card_detail lcd
on lcd.lylty_card_nbr = SFL.lylty_card_nbr
where most_recent = 1
    and lylty_card_status = 1
group by 1
;

/* Partition by SFL and QFF, instead of prioritising QFF */
with sfl as (
    select
    sfl.crn
        , lylty_card_nbr
        , case when sfloptin='true' and sfloptionid<>2 then 'SFL'
            when sfloptin='true' and sfloptionid=2 then 'QFF'
        end as pref
        , trunc(sflpreferenceauditdatetime) sflpreferenceauditdatetime
        , ROW_NUMBER () OVER (
            PARTITION BY sfl.crn, pref
            ORDER BY sflpreferenceauditdatetime desc
        ) AS most_recent
    from loyalty.sfl_preferences as sfl
    where 1=1 
        and sfloptin='true'
        and sfloptionid in (1,2)
)
select
    pref, count(distinct sfl.crn), count(*)
from sfl
inner join loyalty.lylty_card_detail lcd
on lcd.lylty_card_nbr = SFL.lylty_card_nbr
where most_recent = 1
    and lylty_card_status = 1
group by 1
;

select top 188 *
from loyalty.sfl_preferences
where crn = '1000000000000000003'
;

/* Check for difference preferences by crn, card
[EMPTY] -> each crn, card only has one preference */
select top 188
    agg.cnt
    ,bse.*
from loyalty.sfl_preferences as bse
join
(
    select 
        crn
        ,lylty_card_nbr
        ,count(*) as cnt
    from loyalty.sfl_preferences
    group by 1,2
    having cnt > 1
) as agg
on agg.crn = bse.crn
    and agg.lylty_card_nbr = bse.lylty_card_nbr
order by bse.crn, bse.lylty_card_nbr;

select count(*), count(distinct crn)
from loyalty.sfl_preferences as bse;
--^ [2,071,516 | 1,913,476]

select
    sfloptin as sfl_opt_in
    ,partner_abbrev
    ,sfloptionid as sfl_option_id
    ,row_status_ind
    ,count(*)
from loyalty.sfl_preferences
group by 1,2,3,4
order by 1,2,3,4;


-------------------------------------------------
-- SFX opt-in by wks_to_xmas
-------------------------------------------------

select top 18 * 
from loyalty_modeling.sfx_crn_base

select
    date_part('year', sunday_dt) as pref_year
    ,min(sunday_dt), max(sunday_dt)
    ,min(ref_dt), max(ref_dt)
    ,count(*)
from loyalty_modeling.sfx_crn_base
group by 1

select
    sfx_pref_year
    ,wks_to_xmas
    ,avg(is_save_for_xmas::float) as sfx_avg
    ,sum(is_save_for_xmas * smpl_wgt) / sum(smpl_wgt) as sfx_avg_wgt
    ,count(*) as cnt
    ,sum(is_save_for_xmas) as sfx_cnt
from loyalty_modeling.sfx_model_data
group by 1,2
order by 1,2;

select
    wks_to_xmas
    ,avg(is_save_for_xmas::float) as sfx_avg
    ,sum(is_save_for_xmas * smpl_wgt) / sum(smpl_wgt) as sfx_avg_wgt
    ,count(*) as cnt
    ,sum(is_save_for_xmas) as sfx_cnt
from loyalty_modeling.sfx_model_data
group by 1
order by 1;



select *
from loyalty_modeling.sfx_model_data
where wks_to_xmas = 51
limit 100

select *
from loyalty_modeling.sfx_model_data
where is_save_for_xmas = 0
    and sfx_pref_dt between '2019-01-01' and '2019-01-30'
limit 100

select date_diff('week', '2019-01-05'::date, '2019-12-25'::date)

select min(sunday_dt), min(rand_dt), min(ref_dt)
from loyalty_modeling.sfx_crn_base

select top 188 * from loyalty_modeling.sfx_tmp_positive

select min(pref_dt), min(rand_dt), min(ref_dt), max(wks_to_xmas)
from loyalty_modeling.sfx_tmp_positive

select sfx_pref_year
    ,wks_to_xmas
    ,count(*)
from loyalty_modeling.sfx_tmp_positive
group by 1,2
order by 1,2;

select top 188 * from loyalty_modeling.sfx_crn_base


select
    wks_to_xmas
    ,count(*)
from loyalty_modeling.sfx_model_data
group by 1
order by 1;

select
    date_part('month', rand_dt)
    ,count(*)
from loyalty_modeling.sfx_crn_base
cross join loyalty_modeling.sfx_ref as rf0
where (
        rand_dt between rf0.start_dt_2019 and rf0.end_dt_2019
        or rand_dt between rf0.start_dt_2020 and rf0.end_dt_2020
    )
    and rand_dt < '2020-01-01'
group by 1
order by 1

select
    -- date_diff('week', rand_dt, rf.xmas_dt_2019)
    (rf.xmas_dt_2019 - rand_dt) / 7
    ,count(*)
from loyalty_modeling.sfx_crn_base
cross join loyalty_modeling.sfx_ref as rf
where rand_dt < '2020-01-01'
group by 1
order by 1


select date_diff('week', '2019-01-01'::date, '2019-12-01'::date);
select date_diff('week', '2019-01-01'::date, '2019-12-15'::date);
select date_diff('week', '2019-01-01'::date, '2019-12-31'::date);

select
    sfx_pref_year
    ,wks_to_xmas
    ,count(*) as cnt
    ,sum(is_save_for_xmas * smpl_wgt) / sum(smpl_wgt) as sfx_avg_wgt
    ,avg(is_save_for_xmas::float) as sfx_avg
    ,sum(is_save_for_xmas) as sfx_cnt
from loyalty_modeling.sfx_model_data
where is_save_for_xmas = 0
group by 1,2
order by 1,2;

select
    sfx_pref_year
    ,wks_to_xmas
    ,count(*) as cnt
    ,min(sfx_pref_dt)
    ,max(sfx_pref_dt)
    ,max(sfx_pref_dt) - min(sfx_pref_dt) as dt_diff
from loyalty_modeling.sfx_model_data
where is_save_for_xmas = 0
group by 1,2
order by 1,2;


select *
from loyalty_modeling.sfx_model_data
where wks_to_xmas = 0
limit 100

/* Check rand_dt always after eligible date [EMPTY] */
select *
from loyalty_modeling.sfx_crn_base
where rand_dt < eligible_start_dt
limit 10;

/* Check max days ref_dt is behind pref_dt: [20]
-> is_save_for_xmas == 0: pref_dt == rand_dt
-> is_save_for_xmas == 1: pref_dt == preference date for SFX
 */
select
    is_save_for_xmas
    ,max(sfx_pref_dt - ref_dt)
    ,min(sfx_pref_dt - ref_dt)
from loyalty_modeling.sfx_model_data
group by 1
order by 1;

select
    is_save_for_xmas
    ,sfx_pref_dt - ref_dt
    ,count(*)
from loyalty_modeling.sfx_model_data
where sfx_pref_dt - ref_dt > 14
group by 1,2
order by 1,2;

select *
from loyalty_modeling.sfx_model_data
where sfx_pref_dt - ref_dt > 14
limit 100


-------------------------------------------------
-- Positive class: with random ref_dt
-------------------------------------------------
drop table if exists loyalty_modeling.sfx_tmp_positive;
create table loyalty_modeling.sfx_tmp_positive
distkey (crn) as
(
    select
        crn
        ,pref_dt
        ,case
            when date_add('year', -1, rf.xmas_dt_2019) < pref_dt and pref_dt < rf.xmas_dt_2019
                then 2019
            when date_add('year', -1, rf.xmas_dt_2020) < pref_dt and pref_dt < rf.xmas_dt_2020
                then 2020
        end as sfx_pref_year
        ,rf.period_end_dt - pref_dt as period_days0
        ,cast(random() * (period_days0 + 1) as int) as rand_id
        ,null::date as rand_dt
        ,null::int as dow
        ,null::date as ref_dt
        ,case
            when date_add('year', -1, rf.xmas_dt_2019) < pref_dt and pref_dt < rf.xmas_dt_2019
                then datediff('week', pref_dt, rf.xmas_dt_2019)
            when date_add('year', -1, rf.xmas_dt_2020) < pref_dt and pref_dt < rf.xmas_dt_2020
                then datediff('week', pref_dt, rf.xmas_dt_2020)
        end as wks_to_xmas
    from loyalty_modeling.sfx_crn_preference_all
    cross join loyalty_modeling.sfx_ref as rf
    where pref_option = '2. Xmas'
        and pref_dt between rf.period_start_dt and rf.period_end_dt
);
update loyalty_modeling.sfx_tmp_positive
set rand_dt = pref_dt + rand_id
    ,dow = date_part('dow', pref_dt + rand_id)::int
;
update loyalty_modeling.sfx_tmp_positive
set ref_dt = rand_dt - dow - 14
;
select top 18 * from loyalty_modeling.sfx_tmp_positive;

/* num weeks between pref_dt and period_end_dt */
select
    datediff('weeks', bse.rand_dt, rf.period_end_dt) as diff_wks
    ,count(*)
from loyalty_modeling.sfx_tmp_positive as bse
cross join loyalty_modeling.sfx_ref as rf
group by 1
order by 1;

select
    datediff('weeks', bse.pref_dt, rf.period_end_dt) as diff_wks
    ,count(*)
from loyalty_modeling.sfx_tmp_positive as bse
cross join loyalty_modeling.sfx_ref as rf
group by 1
order by 1;

select *
from loyalty_modeling.sfx_tmp_positive as bse
cross join loyalty_modeling.sfx_ref as rf
where datediff('weeks', bse.pref_dt, rf.period_end_dt) >= 80
;

/* year month of pref_dt */
select
    date_part('year', sfx_pref_dt) as pref_year
    ,date_part('month', sfx_pref_dt) as pref_year
    ,count(*)
from loyalty_modeling.sfx_model_data as bse
where is_save_for_xmas = 0 -- 1
group by 1,2
order by 1,2;


-------------------------------------------------
-- cvm 8wk spend by has_pref_change
-------------------------------------------------
select
    is_save_for_xmas
    ,has_pref_change
    ,count(*)
    ,avg(spend_8wk) as spend_8wk
    ,avg(wks_to_xmas) as wks_to_xmas
    ,avg(age) as age
    ,avg(case affluence
        when '1. BUDGET' then 1
        when '2. MAINSTREAM' then 2
        when '3. PREMIUM' then 3
    end) as affluence
    ,avg(case lifestage
        when '01. YOUNG SINGLES/COUPLES' then 1
        when '02. MIDAGE SINGLES/COUPLES' then 2
        when '03. OLDER SINGLES/COUPLES' then 3
        when '04. YOUNG FAMILIES' then 4
        when '05. NEW FAMILIES' then 5
        when '06. OLDER FAMILIES' then 6
        when '07. RETIREES' then 7
    end) as lifestage
from loyalty_modeling.sfx_model_data
group by 1,2
order by 1,2;

select distinct affluence
from loyalty_modeling.sfx_model_data;
select distinct lifestage
from loyalty_modeling.sfx_model_data
order by 1;

-------------------------------------------------
-- Check average last active date by has_pref_change 
-------------------------------------------------
drop table if exists loyalty_modeling.jn_test;
create table loyalty_modeling.jn_test
distkey (crn) as
select
    bse.crn
    ,bse.has_pref_change
    ,act.active_dt::date
    ,isnull(
        datediff('day', act.active_dt::date, '2020-08-01'::date)
        ,365
    ) as days_from_last_active_dt
from loyalty_modeling.sfx_crn_base as bse
left join
(
    select crn, max(active_dt) as active_dt
    from
    (
        /* ET campaign opens */
        select crn, max(ro.event_date) as active_dt
        from loyalty.et_resp_open as ro
        join
            loyalty.et_resp_sendlog as rsl
        on rsl.subscriber_id = ro.subscriber_id
            and rsl.send_id = ro.send_id
        cross join loyalty_modeling.sfx_ref as rf
        where ro.event_date
            between rf.active_period_start_dt
            and rf.period_end_dt
            and right(crn, 5) <> '_TEST'
            and right(crn, 4) <> '_UAT'
        group by 1

        /* Activations */
        union select crn, max(activation_date)
        from loyalty.customer_activated_offers
        cross join loyalty_modeling.sfx_ref as rf
        where activation_date
            between rf.active_period_start_dt
            and rf.period_end_dt
        group by 1
    )z
    group by 1
) as act
on act.crn = bse.crn
;
-- select top 188 * from loyalty_modeling.jn_test;

select
    has_pref_change
    ,count(*) as cnt
    ,avg(days_from_last_active_dt) as days_from_last_active_dt
    ,sum(case when active_dt is null then 1 else 0 end) as missing_activity_cnt
from loyalty_modeling.jn_test
group by 1;
-- 1	37
-- 0	61
--^ has_pref_change == 1: more active closer to 2020-08-01

select
    count(*) as cnt
    ,avg(days_from_last_active_dt) as days_from_last_active_dt
    ,sum(case when active_dt is null then 1 else 0 end) as missing_activity_cnt
from loyalty_modeling.jn_test
;

-------------------------------------------------
-- Check average last active date for SFX opt-in (+ve class)
-------------------------------------------------
drop table if exists loyalty_modeling.jn_test;
create table loyalty_modeling.jn_test
distkey (crn) as
select
    bse.crn
    ,act.active_dt::date
    -- ,isnull(
    --     datediff('day', act.active_dt::date, '2020-08-01'::date)
    --     ,365
    -- ) as days_from_last_active_dt
    ,datediff('day', act.active_dt::date, '2020-08-01'::date) as days_from_last_active_dt
from loyalty_modeling.sfx_model_data as bse
left join
(
    select crn, max(active_dt) as active_dt
    from
    (
        /* ET campaign opens */
        select crn, max(ro.event_date) as active_dt
        from loyalty.et_resp_open as ro
        join
            loyalty.et_resp_sendlog as rsl
        on rsl.subscriber_id = ro.subscriber_id
            and rsl.send_id = ro.send_id
        cross join loyalty_modeling.sfx_ref as rf
        where ro.event_date
            between rf.active_period_start_dt
            and rf.period_end_dt
            and right(crn, 5) <> '_TEST'
            and right(crn, 4) <> '_UAT'
        group by 1

        /* Activations */
        union select crn, max(activation_date)
        from loyalty.customer_activated_offers
        cross join loyalty_modeling.sfx_ref as rf
        where activation_date
            between rf.active_period_start_dt
            and rf.period_end_dt
        group by 1
    )z
    group by 1
) as act
on act.crn = bse.crn
where bse.is_save_for_xmas = 1
;
-- select top 188 * from loyalty_modeling.jn_test;
select
    avg(days_from_last_active_dt)
    ,sum(case when active_dt is null then 1 else 0 end) as missing_activity_cnt
    ,count(*)
from loyalty_modeling.jn_test;
50	137501



-------------------------------------------------
-- Groupings
-------------------------------------------------

SELECT top 18 * from LOYALTY_CAMPAIGN_ANALYTICS.CNA3577;

select lylty_card_status_desc, count(*) from LOYALTY_CAMPAIGN_ANALYTICS.CNA3577 group by 1

select top 18 *
from loyalty_modeling.value_seeking_customers_20200709 as bse

drop table if exists loyalty_modeling.jn_test;
create table loyalty_modeling.jn_test
distkey (crn)
as
select
    isnull(vs.crn, cna.crn::varchar(20)) as crn
    ,vs.value_seeker
    ,cna.email_marketable
    ,cna.has_rewards_app
    ,case when vs.crn is null then 1 else 0 end as is_missing_vs
    ,case when cna.crn is null then 1 else 0 end as is_missing_cna
from
(
    select
        case when (((Pct_on_disc_35pc_flag + Elasticity_20_flag + Pct_budget_item_50pc_flag) >= 2) OR (sow_8wk_dec_10pc_flag = 1))
            then 1 else 0
        end as value_seeker
        ,crn
    from loyalty_modeling.value_seeking_customers_20200709
) as vs
full join
    loyalty_campaign_analytics.cna3577 as cna
on cna.crn = vs.crn
;

select
    case
        when email_marketable = 'Y' and value_seeker = 1 then '1. E-marketable VS'
        when email_marketable <> 'Y' and has_rewards_app = 'Y' and value_seeker = 1 then '2. App-only VS'
        when has_rewards_app = 'Y' and value_seeker = 0 then '3. App-only other'
        when email_marketable <> 'Y' and has_rewards_app <> 'Y' and value_seeker = 1 then '4. Non-marketable VS'
        else 'Other'
    end as segment
    ,count(*) as vol
from loyalty_modeling.jn_test
where is_missing_cna = 0
group by 1
order by 1;
-- segment	vol	
-- 1. E-marketable VS	1437634
-- 2. App-only VS	85336
-- 3. App-only other	410325
-- 4. Non-marketable VS	980305
-- Other	7459730


select 
    is_missing_vs
    ,is_missing_cna
    ,count(*)
from loyalty_modeling.jn_test
group by 1,2
order by 1,2;
-- is_missing_vs	is_missing_cna	count	
-- 0	0	10308567
-- 0	1	2021666
-- 1	0	64763

select
    email_marketable
    ,has_rewards_app
    ,value_seeker
    ,count(*)
    ,count(distinct crn)
from loyalty_modeling.jn_test
group by 1,2,3
order by 1,2,3;


select
    bse.crn
    ,bse.
from loyalty_modeling.value_seeking_customers_20200709 as bse
left join
    loyalty_campaign_analytics.cna3577 as cna
on cna.crn = bse.crn


select column_name
from information_schema.columns
where 1=1
    and table_schema = 'loyalty_campaign_analytics'
    and table_name = 'cna3577'
order by dtd_identifier::int;

select
    email_marketable
    ,has_rapp
    ,has_rewards_app
    ,count(*)
from loyalty_campaign_analytics.cna3577
group by 1,2,3
order by 1,2,3;



-- drop table if exists  loyalty_campaign_analytics.cna3577;
create table loyalty_campaign_analytics.cna3577 as
select * -- select count(*)
from (
select dl.*,
	   case when sfl_opted_in = 'y' then sfl_partner_abbrev else 'auto' end as partner_abbrv,
	   case when pc.src_cstmr_num is not null then 'y' else 'n' end as has_rewards_app,
	   case when email_marketable = 'y' and pref_unsubscribe_comms_frm_edr = 'n' and pref_exl_offer_and_promo_opted_in = 'y' then 'edm' else 'social' end as channel,
	   row_number() over (partition by dl.crn order by lylty_card_actv_date desc) as recent_card -- select count(distinct dl.crn)
from loyalty_campaign_analytics.ca_master_customer_profiling dl
left join loyalty.partner_channel_associate_rewards pc
	   on pc.src_cstmr_num = dl.crn
	  and pc.prtnr_num = '14'
where dl.lylty_card_rgstr_flag = 'y' and dl.lylty_card_status = 1 
  and dl.age_over100_or_missing = 'n' and dl.deceased = 'n'  and dl.age_under16 = 'n' and dl.age_under18 = 'n' 
  and dl.dc_universal_control = 'n'
  and dl.exl_gl_competitor_email_cd12 = 'n'
  and dl.exl_gl_overseas_cd7 = 'n'
  and dl.exl_gl_malicious_first_name_cd8 = 'n'
  and dl.exl_gl_malicious_lname_cd10 ='n'
  and dl.onboarding_cust = 'n'
  and wow_vchr_bal < 4000)
where recent_card = 1
  and partner_abbrv <> 'qff';



select
    case
        when email_marketable = 'Y' and value_seeker = 1 then '1. E-marketable VS'
        when email_marketable <> 'Y' and has_rewards_app = 'Y' and value_seeker = 1 then '2. App-only VS'
        when email_marketable = 'Y' and has_rewards_app = 'Y'  then '3. E-marketable | App | non-VS'
        when email_marketable <> 'Y' and has_rewards_app = 'Y' then '4. Non-E-marketable | App | non-VS'
        when email_marketable <> 'Y' and has_rewards_app <> 'Y' and value_seeker = 1 then '4. Non-marketable VS'
        else 'Other'
    end as segment
    ,count(*)
from loyalty_modeling.jn_test
where is_missing_cna = 0
group by 1
order by 1;



select count(*), count(distinct crn)
from loyalty_campaign_analytics.ca_master_customer_profiling as mcp
where mcp.lylty_card_rgstr_flag = 'Y'
    and mcp.lylty_card_status = 1 
    and mcp.age_over100_or_missing = 'N'
    and mcp.deceased = 'N'
    and mcp.age_under16 = 'N' and mcp.age_under18 = 'N'
    -- and mcp.dc_universal_control = 'n'
    and mcp.exl_gl_competitor_email_cd12 = 'N'
    and mcp.exl_gl_overseas_cd7 = 'N'
    and mcp.exl_gl_malicious_first_name_cd8 = 'N'
    and mcp.exl_gl_malicious_lname_cd10 ='N'
    and mcp.onboarding_cust = 'N'
    and mcp.wow_vchr_bal < 4000

    -- where recent_card = 1
    -- and partner_abbrv <> 'qff';

drop table if exists loyalty_modeling.jn_test;
CREATE TABLE loyalty_modeling.jn_test AS
SELECT * -- SELECT COUNT(*)
FROM (
SELECT DL.crn,
	--    CASE WHEN SFL_OPTED_IN = 'Y' THEN SFL_PARTNER_ABBREV ELSE 'AUTO' END AS PARTNER_ABBRV,
       case when SFL_OPTED_IN = 'Y' and SFL_PARTNER_ABBREV = 'QFF' then 1 else 0 end as PARTNER_ABBRV
	--    CASE WHEN PC.SRC_CSTMR_NUM IS NOT NULL THEN 'Y' ELSE 'N' END AS HAS_REWARDS_APP,
	--    CASE WHEN EMAIL_MARKETABLE = 'Y' AND PREF_UNSUBSCRIBE_COMMS_FRM_EDR = 'N' AND PREF_EXL_OFFER_AND_PROMO_OPTED_IN = 'Y' THEN 'eDM' ELSE 'SOCIAL' END AS CHANNEL,
	--    ROW_NUMBER() OVER (PARTITION BY DL.CRN ORDER BY LYLTY_CARD_ACTV_DATE DESC) AS RECENT_CARD -- SELECT COUNT(DISTINCT DL.CRN)
FROM LOYALTY_CAMPAIGN_ANALYTICS.CA_MASTER_CUSTOMER_PROFILING DL
-- LEFT JOIN LOYALTY.PARTNER_CHANNEL_ASSOCIATE_REWARDS PC
-- 	   ON PC.SRC_CSTMR_NUM = DL.CRN
-- 	  AND PC.PRTNR_NUM = '14'
WHERE DL.LYLTY_CARD_RGSTR_FLAG = 'Y' AND DL.LYLTY_CARD_STATUS = 1 
  AND DL.AGE_OVER100_OR_MISSING = 'N' AND DL.DECEASED = 'N'  AND DL.AGE_UNDER16 = 'N' AND DL.AGE_UNDER18 = 'N' 
  AND DL.DC_UNIVERSAL_CONTROL = 'N'
  AND DL.EXL_GL_COMPETITOR_EMAIL_CD12 = 'N'
  AND DL.EXL_GL_OVERSEAS_CD7 = 'N'
  AND DL.EXL_GL_MALICIOUS_FIRST_NAME_CD8 = 'N'
  AND DL.EXL_GL_MALICIOUS_LNAME_CD10 ='N'
  AND DL.ONBOARDING_CUST = 'N'
  AND WOW_VCHR_BAL < 4000)
-- WHERE RECENT_CARD = 1
--   AND PARTNER_ABBRV <> 'QFF';
;

select PARTNER_ABBRV, count(*), count(distinct crn)
from loyalty_modeling.jn_test
group by 1;

select count(*), count(distinct crn)
from LOYALTY_CAMPAIGN_ANALYTICS.CNA3577;




