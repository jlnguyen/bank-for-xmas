--==============================================================
/*
SCRIPT:
    a01_save_for_xmas_prep.sql

PURPOSE:
    - Save for xmas
    - Two options for claiming reward points:
        1. Auto-redeem $10 from 2000 pts
        2. Save up for December (xmas)
    - Analytics have shown that customers who claim option 2 spend
    more than option 1 during xmas
    - We identify customers who switch from option 1 to 2 to build a
    model to target customers with high propensity to switch

    -- CMD2 backfilled to 2018-10-21
    -- CMD2 filled to 2020-06-28 (07Jul2020)

INPUTS:

OUTPUTS:

DEVELOPMENT:
    Version 1.0 - Joseph Nguyen | 01Jul2020
    ---
    Version 1.1 - Joseph Nguyen | 28Jul2020
        - SFX opt-in date == model date
        - AKL time holdout training end date: 2020-05-01
    ---
    Version 1.2 - Joseph Nguyen | 30Jul2020
        - Exclude all customers who change any claim preference
        from negative class
    ---
    Version 1.3 - Joseph Nguyen | 31Jul2020
        - Revert v1.2: randomly sample any customer for negative
        class, regardless of auto-10, QFF pref change
            -> excluding these customers results in a bias with
            more missing CVM data
    ---
*/
--==============================================================
-------------------------------------------------

-------------------------------------------------
-- Preprocess
-------------------------------------------------
drop table if exists loyalty_modeling.sfx_ref;
create table loyalty_modeling.sfx_ref as
(
    select
    date_trunc('week', '2019-12-31'::date)::date - 1 as xmas_dt_2019 -- sunday
    ,date_trunc('week', '2020-12-31'::date)::date - 1 as xmas_dt_2020 -- sunday
    ,dateadd('year', -1, xmas_dt_2019)::date + 1 as period_start_dt
    ,'2020-08-22'::date as period_end_dt    -- saturday
    ,12 as active_period_mths   -- cover pre-COVID-19 period (2020-03-01 to 2020-07-01)
    ,dateadd('month', -active_period_mths, period_end_dt)::date as active_period_start_dt
    ,period_end_dt - period_start_dt as period_days
    ,1e6::int as n_sample_neg
);
-- select * from loyalty_modeling.sfx_ref;

-------------------------------------------------
-- Crns who change claim preference
-------------------------------------------------
drop table if exists loyalty_modeling.sfx_crn_preference_all;
create table loyalty_modeling.sfx_crn_preference_all
distkey (crn) as
select
    z.*
    -- ,pref_dt - date_part('dow', pref_dt) - 14 as ref_dt -- Sunday: prior 2 weeks
    -- ,date_trunc('week', pref_dt)::date - 15 as ref_dt -- Sunday: prior 2 weeks
from (
    select
        sfl.crn
        ,sfl.lylty_card_nbr
        ,case
            when sfloptin = 'false' and sfloptionid is null then '1. Auto'
            when sfloptin = 'true' and sfloptionid <> 2 then '2. Xmas'
            when sfloptin = 'true' and sfloptionid = 2 then '3. QFF'
            else NULL
        end as pref_option
        ,sfl.sflpreferenceauditdatetime::date as pref_dt
        ,sfl.row_status_ind
        ,row_number() over (
            partition by sfl.crn
            order by pref_dt desc
        ) as latest_pref_id
    from loyalty.sfl_preferences as sfl
    left join
        loyalty.lylty_card_detail as lcd
    on lcd.crn = sfl.crn
        and lcd.lylty_card_nbr = sfl.lylty_card_nbr
    where lcd.lylty_card_status = 1
        and lcd.lylty_card_rgstr_flag = 'Y'
        -- and sfl.sfloptin = 'true'
        -- and sfl.sfloptionid in (1, 2)
)z
cross join loyalty_modeling.sfx_ref as rf
where latest_pref_id = 1
-- where ref_dt >= rf.period_start_dt
--     and latest_pref_id = 1
;
grant select on loyalty_modeling.sfx_crn_preference_all to public;
select count(*), count(distinct crn) from loyalty_modeling.sfx_crn_preference_all; -- [1,838,824 | 1,838,824]

/* select pref_option, count(*)
from loyalty_modeling.sfx_crn_preference_all
group by 1
order by 1; */
-- 1. Auto	230,510
-- 2. Xmas	390,796
-- 3. QFF	1,217,518


--==============================================================
/*
    Get active crns
*/
--==============================================================
set seed to 0.25;
drop table if exists loyalty_modeling.sfx_crn_base;
create table loyalty_modeling.sfx_crn_base
distkey (crn)
as
with active as
(
    select crn
    from loyalty_campaign_analytics.ca_master_customer_profiling as mcp
    where mcp.lylty_card_rgstr_flag = 'Y'
        and mcp.lylty_card_status = 1 
        and mcp.age_over100_or_missing = 'N'
        and mcp.deceased = 'N'
        and mcp.age_under16 = 'N' and mcp.age_under18 = 'N'
        and mcp.dc_universal_control = 'N'
        and mcp.exl_gl_competitor_email_cd12 = 'N'
        and mcp.exl_gl_overseas_cd7 = 'N'
        and mcp.exl_gl_malicious_first_name_cd8 = 'N'
        and mcp.exl_gl_malicious_lname_cd10 ='N'
        and mcp.onboarding_cust = 'N'
        and mcp.wow_vchr_bal < 4000
)
,eligible as
(
    select distinct
        act.crn
        ,rf.period_start_dt as eligible_start_dt
        -- ,case when sfx.pref_dt >= rf.period_start_dt
        --     then sfx.pref_dt
        --     else rf.period_start_dt
        -- end as eligible_start_dt
        ,rf.period_end_dt - eligible_start_dt as period_days
        ,sfx.pref_dt
        ,sfx.pref_option
        ,case when sfx.crn is not null then 1 else 0 end as has_pref_change
    from active as act
    left join
        loyalty_modeling.sfx_crn_preference_all as sfx
    on sfx.crn = act.crn
    cross join loyalty_modeling.sfx_ref as rf
    where 1=1
        -- and sfx.crn is null  -- exclude crns with any pref change
        and isnull(sfx.pref_option, '') not in ('3. QFF')
        -- and isnull(sfx.pref_dt, rf.period_end_dt) <= rf.period_end_dt
)
-- select count(*) from eligible
select
    crn
    ,cast(random() * (period_days + 1) as int) as rand_id
    -- ,eligible_start_dt + rand_id as rand_dt
    -- ,date_part('dow', rand_dt)::int as dow
    -- ,rand_dt - dow as sunday_dt
    -- ,sunday_dt - 14 as ref_dt
    ,eligible_start_dt
    ,pref_dt
    ,pref_option
    ,period_days
    ,null::date as rand_dt
    ,null::int as dow
    ,null::date as sunday_dt
    ,null::int as sunday_year
    ,null::date as ref_dt
    ,has_pref_change
from eligible
;
grant select on loyalty_modeling.sfx_crn_base to public;
-- select count(*), min(pref_dt), max(pref_dt) from loyalty_modeling.sfx_crn_base;  -- [10,007,179 | 0017-04-27 | 2020-08-27]

-------------------------------------------------
-- ref_dt: necessary to partition because rand_id changes in calculations
-------------------------------------------------
update loyalty_modeling.sfx_crn_base
set rand_dt = eligible_start_dt + rand_id
    ,dow = date_part('dow', eligible_start_dt + rand_id)::int
;
update loyalty_modeling.sfx_crn_base
set sunday_dt = rand_dt - dow
    ,ref_dt = rand_dt - dow - 14
;
update loyalty_modeling.sfx_crn_base
set sunday_year = date_part('year', sunday_dt)
;
-- delete from loyalty_modeling.sfx_crn_base
-- where exists (
--     select 1
--     from loyalty_modeling.sfx_ref as rf
--     where loyalty_modeling.sfx_crn_base.sunday_year < date_part('year', rf.period_start_dt)
-- );
--^ [41,362 rows affected]

select count(*), min(ref_dt), max(ref_dt), min(sunday_dt), max(sunday_dt) from loyalty_modeling.sfx_crn_base;
--^ [4,982,931	2018-12-16	2020-06-28	2018-12-30	2020-07-12]
--^ [10,376,663	2018-12-16	2020-08-02	2018-12-30	2020-08-16]
select eligible_start_dt, count(*) from loyalty_modeling.sfx_crn_base group by 1 order by 1;
-- [2018-12-30	4,082,491]
-- [2018-12-30	10,376,663]
select has_pref_change, count(*) from loyalty_modeling.sfx_crn_base group by 1 order by 1;
select pref_option, count(*) from loyalty_modeling.sfx_crn_base group by 1 order by 1;


--==============================================================
/*
    Modelling data
*/
--==============================================================
drop table if exists loyalty_modeling.sfx_model_data;
create table loyalty_modeling.sfx_model_data
(
    crn                         varchar(20)
    ,ref_dt                     date
    ,is_save_for_xmas           smallint
    ,smpl_wgt                   float   --numeric(12,5)

    ,sfx_pref_dt                date
    ,sfx_pref_year              int
    ,wks_to_xmas                int
    ,has_pref_change            int

    /* dim_cust features */
    ,age                        smallint
    ,age_band                   varchar(20)
    ,gender                     char(1)
    ,postcode                   char(4)
    /* cvm features */
    ,macro_segment_curr         varchar(20)
    ,macro_segment_prev         varchar(20)
    ,duration_macro_curr        smallint
    ,tenure                     smallint
    ,tenure_band                varchar(20)
    ,preferred_store            varchar(20)
    ,affluence                  varchar(50)
    ,lifestage                  varchar(50)
    ,spend_8wk                  numeric(10,2) default 0
    ,stretch_perc_raw           numeric(5,2) default 0
)
distkey (crn)
;
grant select on loyalty_modeling.sfx_model_data to public;

-------------------------------------------------
-- Positive class: with random ref_dt
-------------------------------------------------
-- delete from loyalty_modeling.sfx_model_data;
drop table if exists loyalty_modeling.sfx_tmp_positive;
create table loyalty_modeling.sfx_tmp_positive
distkey (crn) as
(
    select
        crn
        ,pref_dt
        ,pref_dt - date_part('dow', pref_dt)::int - 14 as ref_dt -- Sunday: prior 2 weeks
        ,case
            when date_add('year', -1, rf.xmas_dt_2019) < pref_dt and pref_dt < rf.xmas_dt_2019
                then 2019
            when date_add('year', -1, rf.xmas_dt_2020) < pref_dt and pref_dt < rf.xmas_dt_2020
                then 2020
        end as sfx_pref_year
        ,case
            when date_add('year', -1, rf.xmas_dt_2019) < pref_dt and pref_dt < rf.xmas_dt_2019
                then datediff('week', pref_dt, rf.xmas_dt_2019)
            when date_add('year', -1, rf.xmas_dt_2020) < pref_dt and pref_dt < rf.xmas_dt_2020
                then datediff('week', pref_dt, rf.xmas_dt_2020)
        end as wks_to_xmas
    from loyalty_modeling.sfx_crn_base
    -- from loyalty_modeling.sfx_crn_preference_all
    cross join loyalty_modeling.sfx_ref as rf
    where pref_option = '2. Xmas'
        and pref_dt between rf.period_start_dt and rf.period_end_dt
);
select count(*) from loyalty_modeling.sfx_tmp_positive;
-- update loyalty_modeling.sfx_tmp_positive
-- set rand_dt = pref_dt + rand_id
--     ,dow = date_part('dow', pref_dt + rand_id)::int
-- ;
-- update loyalty_modeling.sfx_tmp_positive
-- set ref_dt = rand_dt - dow - 14
-- ;

/*
-- Checks
select
    sfx_pref_year
    ,date_part('dow', ref_dt)::int
    ,min(pref_dt), max(pref_dt)
    ,min(ref_dt), max(ref_dt)
    ,count(*)
from loyalty_modeling.sfx_tmp_positive
group by 1,2
; */

insert into loyalty_modeling.sfx_model_data
(
    crn, ref_dt, is_save_for_xmas, smpl_wgt
    ,sfx_pref_dt
    ,sfx_pref_year
    ,wks_to_xmas
    ,has_pref_change
)
select
    crn
    ,ref_dt
    ,1 as is_save_for_xmas
    ,1 as smpl_wgt
    ,pref_dt
    ,sfx_pref_year
    ,wks_to_xmas
    ,0 as has_pref_change
from loyalty_modeling.sfx_tmp_positive
;
--^ [147,467 rows affected]

-------------------------------------------------
-- Sample negative class
-------------------------------------------------
-- delete from loyalty_modeling.sfx_model_data where is_save_for_xmas = 0;
set seed to 0.25;
insert into loyalty_modeling.sfx_model_data
(
    crn
    ,ref_dt
    ,is_save_for_xmas
    ,smpl_wgt
    ,sfx_pref_dt
    ,sfx_pref_year
    ,wks_to_xmas
    ,has_pref_change
)
select
    bse.crn
    ,bse.ref_dt
    ,0 as is_save_for_xmas
    ,agg.tot::float / rf.n_sample_neg as smpl_wgt
    ,bse.rand_dt as sfx_pref_dt
    ,case
        when date_add('year', -1, rf.xmas_dt_2019) < bse.rand_dt and bse.rand_dt < rf.xmas_dt_2019
            then 2019
        when date_add('year', -1, rf.xmas_dt_2020) < bse.rand_dt and bse.rand_dt < rf.xmas_dt_2020
            then 2020
    end as sfx_pref_year
    -- ,date_part('year', bse.rand_dt) as sfx_pref_year
    ,case
        when date_add('year', -1, rf.xmas_dt_2019) < bse.rand_dt and bse.rand_dt < rf.xmas_dt_2019
            then datediff('week', bse.rand_dt, rf.xmas_dt_2019)
        when date_add('year', -1, rf.xmas_dt_2020) < bse.rand_dt and bse.rand_dt < rf.xmas_dt_2020
            then datediff('week', bse.rand_dt, rf.xmas_dt_2020)
    end as wks_to_xmas
    ,bse.has_pref_change
from
(
    select *
        ,row_number() over (order by random()) as rand_id1
    from loyalty_modeling.sfx_crn_base
    where isnull(pref_option, '') <> '2. Xmas'
    /* where has_pref_change = 0 */
) as bse
cross join loyalty_modeling.sfx_ref as rf
cross join (
    select count(*) as tot
    from loyalty_modeling.sfx_crn_base
    where isnull(pref_option, '') <> '2. Xmas'
) as agg
where bse.rand_id1 <= rf.n_sample_neg
;
--^ [1,000,000 rows affected]
select
    is_save_for_xmas, smpl_wgt, count(*)
    , min(ref_dt), max(ref_dt)
    , min(sfx_pref_dt), max(sfx_pref_dt)
from loyalty_modeling.sfx_model_data
group by 1,2 order by 1,2
;
-- 0	1000000	2018-12-16	2020-07-05	2018-12-30	2020-07-20
-- 1	139085	2018-12-16	2020-07-05	2018-12-30	2020-07-20
-- 0	10.007179	1000000	2018-12-16	2020-08-02	2018-12-30	2020-08-22
-- 1	1	        147467	2018-12-16	2020-08-02	2018-12-30	2020-08-22


-------------------------------------------------
-- dim_cust
-------------------------------------------------
update loyalty_modeling.sfx_model_data
set age = calc.age
    ,age_band = case
        when calc.age = 0 then 'Missing'
        when calc.age < 20 then '1. < 20'
        when calc.age < 25 then '2. [20, 25)'
        when calc.age < 30 then '3. [25, 30)'
        when calc.age < 35 then '4. [30, 35)'
        when calc.age < 45 then '5. [35, 45)'
        when calc.age < 55 then '6. [45, 55)'
        when calc.age < 65 then '7. [55, 65)'
        when calc.age < 75 then '8. [65, 75)'
        else '9. >= 75'
    end
    ,gender = case
        when upper(calc.gender) = 'F'
            or trim(calc.title_desc) in (
                'Miss'
                ,'Mrs.'
                ,'Ms.'
                ,'Lady'
                ,'Sister'
            )
            then 'F'
        when upper(calc.gender) = 'M'
            or trim(calc.title_desc) in (
                'Mr.'
                ,'Father'
                ,'Sir'
            )
            then 'M'
        else NULL
    end
    ,postcode = case when len(calc.cust_mail_addr_postcode) = 4
        then nullif(regexp_substr(
            calc.cust_mail_addr_postcode,
            '(\\d{4})', 1,1,'e'), '')
        end
-- select count(*), count(distinct upd.crn)
from loyalty_modeling.sfx_model_data as upd
join
(
    select distinct     -- multiple crns from dch (1 case)
        bse.crn
        ,coalesce(dch.age, dc.age, 0) as age
        ,coalesce(dch.gender, dc.gender) as gender
        ,coalesce(dch.title_desc, dc.title_desc) as title_desc
        ,coalesce(
            dch.cust_mail_addr_postcode
            ,dc.cust_mail_addr_postcode
        ) as cust_mail_addr_postcode
    from loyalty_modeling.sfx_model_data as bse
    left join
        loyalty.dim_cust_hist as dch
    on dch.crn = bse.crn
        and bse.ref_dt between dch.start_date and dch.end_date
    left join
        loyalty.dim_cust as dc
    on dc.crn = bse.crn
    -- where bse.crn = '1000000000001705548'
) as calc
on calc.crn = upd.crn
;
--^ [1,147,467 rows affected]

-------------------------------------------------
-- cvm
-------------------------------------------------
update loyalty_modeling.sfx_model_data
set macro_segment_curr = calc.macro_segment_curr
    ,macro_segment_prev = calc.macro_segment_prev
    ,duration_macro_curr = calc.duration_macro_curr
    ,tenure = calc.tenure
    ,tenure_band = calc.tenure_band
    ,preferred_store = calc.preferred_store
    ,affluence = calc.affluence
    ,lifestage = calc.lifestage
    ,spend_8wk = calc.spend_8wk
    ,stretch_perc_raw = calc.stretch_perc_raw
from
    loyalty_modeling.sfx_model_data as upd
join
(
    select
        bse.crn
        ,case cvm.macro_segment_curr
            when 'LAPSED' then '01. LAPSED'
            when 'INACTIVE' then '02. INACTIVE'
            when 'LOW' then '03. LOW'
            when 'LVLF' then '04. LVLF'
            when 'LVLFB' then '05. LVLFB'
            when 'LVHFA' then '06. LVHFA'
            when 'LVHFB' then '07. LVHFB'
            when 'MVMEDA' then '08. MVMEDA'
            when 'MVMEDB' then '09. MVMEDB'
            when 'MVHIGH' then '10. MVHIGH'
            when 'HVMED' then '11. HVMED'
            when 'HVHIGH' then '12. HVHIGH'
            else NULL
        end as macro_segment_curr
        ,cvm.macro_segment_prev
        ,cvm.duration_macro_curr
        ,cvm.tenure
        ,case when cvm.tenure / 365 between 0 and 1 then '1. [0, 1]'
            when cvm.tenure / 365 between 1 and 2 then '2. (1, 2]'
            when cvm.tenure / 365 between 2 and 3 then '3. (2, 3]'
            when cvm.tenure / 365 between 3 and 5 then '4. (3, 5]'
            when cvm.tenure / 365 between 5 and 10 then '5. (5, 10]'
            when cvm.tenure / 365 > 10 then '6. > 10'
        end as tenure_band
        ,cvm.preferred_store
        ,case cvm.affluence
            when 'BUDGET' then '1. BUDGET'
            when 'MAINSTREAM' then '2. MAINSTREAM'
            when 'PREMIUM' then '3. PREMIUM'
            else NULL
        end as affluence
        ,case cvm.lifestage
            when 'YOUNG SINGLES/COUPLES' then '01. YOUNG SINGLES/COUPLES'
            when 'MIDAGE SINGLES/COUPLES' then '02. MIDAGE SINGLES/COUPLES'
            when 'OLDER SINGLES/COUPLES' then '03. OLDER SINGLES/COUPLES'
            when 'YOUNG FAMILIES' then '04. YOUNG FAMILIES'
            when 'NEW FAMILIES' then '05. NEW FAMILIES'
            when 'OLDER FAMILIES' then '06. OLDER FAMILIES'
            when 'RETIREES' then '07. RETIREES'
            else NULL
        end as lifestage
        ,coalesce(cvm."8wk_spend", 0) as spend_8wk
        ,cvm.stretch_perc_raw
    from loyalty_modeling.sfx_model_data as bse
    join
        loyalty.customer_value_model as cvm
    on cvm.crn = bse.crn
        and bse.ref_dt - 5 = cvm.pw_end_date::date
) as calc
on calc.crn = upd.crn
;
--^ [1,070,526 rows affected]

/* delete tmp tables */
drop table if exists loyalty_modeling.sfx_tmp_positive;


--==============================================================
/*
    Export
*/
--==============================================================
UNLOAD ('select * from loyalty_modeling.sfx_model_data')
TO 's3://data-preprod-redshift-exports/Joe/sfx_model_data'
CREDENTIALS 'aws_access_key_id=AKIA4KLQCVHRE53KRCQK;aws_secret_access_key=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'
FORMAT PARQUET
ALLOWOVERWRITE
PARALLEL OFF;

-------------------------------------------------
-- QA
-------------------------------------------------
select count(*), min(ref_dt), max(ref_dt)
    ,sum(case when gender is null then 1 else 0 end) as gender
    ,sum(case when macro_segment_curr is null then 1 else 0 end) as macro_segment_curr
    ,sum(case when duration_macro_curr is null then 1 else 0 end) as duration_macro_curr
    ,sum(case when tenure is null then 1 else 0 end) as tenure
    ,sum(case when tenure_band is null then 1 else 0 end) as tenure_band
    ,sum(case when affluence is null then 1 else 0 end) as affluence
    ,sum(case when lifestage is null then 1 else 0 end) as lifestage
from loyalty_modeling.sfx_model_data
;
-- [1,137,501	2018-12-16	2020-06-28	154000	111206	111206	111206	111206	214272	303863]
-- [1,147,467	2018-12-16	2020-08-02	121438	76941	76941	76941	76941	227764	428499]
-- or gender is null               -- [154,696] | [171,466] | [153,858]
-- or macro_segment_curr is null   -- [84,694] |  [121,350] | [111,427]
-- or duration_macro_curr is null  -- [84,694]
-- or tenure is null               -- [84,694]
-- or tenure_band is null
-- or affluence is null            -- [198,338] | [236,760] | [214,780]
-- or lifestage is null            -- [288,832] | [338,553] | [304,504]
;
--^ [1,147,119	1,147,119	2018-10-21	2020-06-14]
--^ [1,137,958	1,137,958	2018-12-16	2020-06-28]

/* create table loyalty_modeling.sfx_model_data_20200731 as (
    select * from loyalty_modeling.sfx_model_data
); */

select is_save_for_xmas, smpl_wgt, has_pref_change, count(*), min(ref_dt), max(ref_dt)
from loyalty_modeling.sfx_model_data
group by 1,2,3
order by 1,2,3
;
select
    count(*) as cnt
    ,sum(is_save_for_xmas * smpl_wgt) / sum(smpl_wgt) as sfx_avg_wgt
    ,avg(is_save_for_xmas::float) as sfx_avg
from loyalty_modeling.sfx_model_data
;
select
    gender
    ,macro_segment_curr
    ,affluence
    ,lifestage
    ,count(*)
from loyalty_modeling.sfx_model_data
group by 1,2,3,4
order by 1,2,3,4;

select
    is_save_for_xmas
    ,ref_dt
    ,count(*)
from loyalty_modeling.sfx_model_data
group by 1,2
order by 1,2
;

select
    sfx_pref_year
    ,wks_to_xmas
    ,count(*) as cnt
    ,sum(is_save_for_xmas * smpl_wgt) / sum(smpl_wgt) as sfx_avg_wgt
    ,avg(is_save_for_xmas::float) as sfx_avg
    ,sum(is_save_for_xmas) as sfx_cnt
    ,min(sfx_pref_dt)
    ,max(sfx_pref_dt)
    ,max(sfx_pref_dt) - min(sfx_pref_dt) as dt_diff
from loyalty_modeling.sfx_model_data
group by 1,2
order by 1,2;

/* gender */
select cn.title_desc, cn.gender, bse.*
from loyalty_modeling.sfx_model_data as bse
left join
    loyalty.dim_cust as cn
on cn.crn = bse.crn
where cn.title_desc is not null
    and bse.gender is null
limit 100;

/* Check rand_dt always after eligible date [EMPTY] */
select *
from loyalty_modeling.sfx_crn_base
where rand_dt < eligible_start_dt
limit 10;

/* Check max days ref_dt is behind pref_dt: [20]
-> is_save_for_xmas == 0: pref_dt == rand_dt
-> is_save_for_xmas == 1: pref_dt == preference date for SFX
 */
select
    is_save_for_xmas
    ,max(sfx_pref_dt - ref_dt)
    ,min(sfx_pref_dt - ref_dt)
from loyalty_modeling.sfx_model_data
group by 1
order by 1;

select
    sfx_pref_dt - ref_dt, *
from loyalty_modeling.sfx_model_data
limit 10;

/* num weeks between pref_dt and period_end_dt */
select
    is_save_for_xmas
    ,datediff('weeks', bse.sfx_pref_dt, rf.period_end_dt) as diff_wks
    -- ,datediff('weeks', bse.sfx_pref_dt, rf.period_end_dt) as diff_wks
    ,count(*)
from loyalty_modeling.sfx_model_data as bse
cross join loyalty_modeling.sfx_ref as rf
group by 1,2
order by 1,2;