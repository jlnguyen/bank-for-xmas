--==============================================================
/*
SCRIPT:
    a02_sfx_score_base.sql

PURPOSE:
    - Save for xmas
    - Get score base
        - Exclude crns who have opt-in to SFX

INPUTS:

OUTPUTS:

DEVELOPMENT:
    Version 1.0 - Joseph Nguyen | 13Jul2020
    ---
*/
--==============================================================
-------------------------------------------------

-------------------------------------------------
-- Preprocess
-------------------------------------------------
drop table if exists loyalty_modeling.sfx_ref_scr;
create table loyalty_modeling.sfx_ref_scr as
(
    select
    '2020-08-23'::date as ref_dt
    ,date_trunc('week', '2020-12-31'::date)::date - 1 as xmas_dt_2020 -- sunday
);
-- select * from loyalty_modeling.sfx_ref_scr;


--==============================================================
/*
    Scoring data
*/
--==============================================================
drop table if exists loyalty_modeling.sfx_score_base;
create table loyalty_modeling.sfx_score_base
(
    crn                         varchar(20)
    ,ref_dt                     date
    ,target_segment             varchar(40)

    ,sfx_pref_dt                date
    ,sfx_pref_year              int
    ,wks_to_xmas                int

    /* dim_cust features */
    ,age                        smallint
    ,age_band                   varchar(20)
    ,gender                     char(1)
    ,postcode                   char(4)
    /* cvm features */
    ,macro_segment_curr         varchar(20)
    ,macro_segment_prev         varchar(20)
    ,duration_macro_curr        smallint
    ,tenure                     smallint
    ,tenure_band                varchar(20)
    ,preferred_store            varchar(20)
    ,affluence                  varchar(50)
    ,lifestage                  varchar(50)
    ,spend_8wk                  numeric(10,2) default 0
)
distkey (crn)
;
grant select on loyalty_modeling.sfx_score_base to public;

-- delete from loyalty_modeling.sfx_score_base;
insert into loyalty_modeling.sfx_score_base (crn, ref_dt, target_segment)
select
    cna.crn
    ,rf.ref_dt
    ,case
        when cna.email_marketable = 'Y' and vs.is_value_seeker = 1
            then '1. E-marketable VS'
        when cna.email_marketable <> 'Y' and cna.has_rewards_app = 'Y'
            and vs.is_value_seeker = 1
                then '2. App-only VS'
        when cna.email_marketable = 'Y' and cna.has_rewards_app = 'Y'
            and isnull(vs.is_value_seeker, 0) = 0
                then '3. E-marketable | App | non-VS'
        when cna.email_marketable <> 'Y' and cna.has_rewards_app = 'Y'
            and isnull(vs.is_value_seeker, 0) = 0
                then '4. Non-E-marketable | App | non-VS'
        when cna.email_marketable <> 'Y' and cna.has_rewards_app <> 'Y'
            and vs.is_value_seeker = 1
                then '5. Non-marketable VS'
        else 'Other'
    end as target_segment
from loyalty_campaign_analytics.cna3577 as cna
cross join loyalty_modeling.sfx_ref_scr as rf
left join
(
    select
        crn
        ,case when (((Pct_on_disc_35pc_flag + Elasticity_20_flag + Pct_budget_item_50pc_flag) >= 2) OR (sow_8wk_dec_10pc_flag = 1))
            then 1 else 0
        end as is_value_seeker
    from loyalty_modeling.value_seeking_customers_20200709
) as vs
on vs.crn = cna.crn;
--^ [10,373,330 rows affected]

-------------------------------------------------
-- wks_to_xmas
-------------------------------------------------
update loyalty_modeling.sfx_score_base
set sfx_pref_dt = calc.sfx_pref_dt0
    ,sfx_pref_year = calc.sfx_pref_year
    ,wks_to_xmas = calc.wks_to_xmas
from loyalty_modeling.sfx_score_base as upd
join
(
    select
        bse.crn
        ,date_add('week', 2, bse.ref_dt) as sfx_pref_dt0
        ,date_part('year', sfx_pref_dt0) as sfx_pref_year
        ,datediff('week', sfx_pref_dt0, rf.xmas_dt_2020) as wks_to_xmas
    from loyalty_modeling.sfx_score_base as bse
    cross join loyalty_modeling.sfx_ref_scr as rf
) as calc
on calc.crn = upd.crn;

-------------------------------------------------
-- dim_cust
-------------------------------------------------
update loyalty_modeling.sfx_score_base
set age = calc.age
    ,age_band = case
        when calc.age = 0 then 'Missing'
        when calc.age < 20 then '1. < 20'
        when calc.age < 25 then '2. [20, 25)'
        when calc.age < 30 then '3. [25, 30)'
        when calc.age < 35 then '4. [30, 35)'
        when calc.age < 45 then '5. [35, 45)'
        when calc.age < 55 then '6. [45, 55)'
        when calc.age < 65 then '7. [55, 65)'
        when calc.age < 75 then '8. [65, 75)'
        else '9. >= 75'
    end
    ,gender = case
        when upper(calc.gender) = 'F'
            or trim(calc.title_desc) in (
                'Miss'
                ,'Mrs.'
                ,'Ms.'
                ,'Lady'
                ,'Sister'
            )
            then 'F'
        when upper(calc.gender) = 'M'
            or trim(calc.title_desc) in (
                'Mr.'
                ,'Father'
                ,'Sir'
            )
            then 'M'
        else NULL
    end
    ,postcode = case when len(calc.cust_mail_addr_postcode) = 4
        then nullif(regexp_substr(
            calc.cust_mail_addr_postcode,
            '(\\d{4})', 1,1,'e'), '')
        end
-- select count(*), count(distinct upd.crn)
from loyalty_modeling.sfx_score_base as upd
join
(
    select distinct     -- multiple crns from dch (1 case)
        bse.crn
        ,coalesce(dch.age, dc.age, 0) as age
        ,coalesce(dch.gender, dc.gender) as gender
        ,coalesce(dch.title_desc, dc.title_desc) as title_desc
        ,coalesce(
            dch.cust_mail_addr_postcode
            ,dc.cust_mail_addr_postcode
        ) as cust_mail_addr_postcode
    from loyalty_modeling.sfx_score_base as bse
    left join
        loyalty.dim_cust_hist as dch
    on dch.crn = bse.crn
        and bse.ref_dt between dch.start_date and dch.end_date
    left join
        loyalty.dim_cust as dc
    on dc.crn = bse.crn
) as calc
on calc.crn = upd.crn
;
--^ [4,555,057 rows affected]

-------------------------------------------------
-- cvm
-------------------------------------------------
update loyalty_modeling.sfx_score_base
set macro_segment_curr = calc.macro_segment_curr
    ,macro_segment_prev = calc.macro_segment_prev
    ,duration_macro_curr = calc.duration_macro_curr
    ,tenure = calc.tenure
    ,tenure_band = calc.tenure_band
    ,preferred_store = calc.preferred_store
    ,affluence = calc.affluence
    ,lifestage = calc.lifestage
    ,spend_8wk = calc.spend_8wk
from
    loyalty_modeling.sfx_score_base as upd
join
(
    select
        bse.crn
        ,case cvm.macro_segment_curr
            when 'LAPSED' then '01. LAPSED'
            when 'INACTIVE' then '02. INACTIVE'
            when 'LOW' then '03. LOW'
            when 'LVLF' then '04. LVLF'
            when 'LVLFB' then '05. LVLFB'
            when 'LVHFA' then '06. LVHFA'
            when 'LVHFB' then '07. LVHFB'
            when 'MVMEDA' then '08. MVMEDA'
            when 'MVMEDB' then '09. MVMEDB'
            when 'MVHIGH' then '10. MVHIGH'
            when 'HVMED' then '11. HVMED'
            when 'HVHIGH' then '12. HVHIGH'
            else NULL
        end as macro_segment_curr
        ,cvm.macro_segment_prev
        ,cvm.duration_macro_curr
        ,cvm.tenure
        ,case when cvm.tenure / 365 between 0 and 1 then '1. [0, 1]'
            when cvm.tenure / 365 between 1 and 2 then '2. (1, 2]'
            when cvm.tenure / 365 between 2 and 3 then '3. (2, 3]'
            when cvm.tenure / 365 between 3 and 5 then '4. (3, 5]'
            when cvm.tenure / 365 between 5 and 10 then '5. (5, 10]'
            when cvm.tenure / 365 > 10 then '6. > 10'
        end as tenure_band
        ,cvm.preferred_store
        ,case cvm.affluence
            when 'BUDGET' then '1. BUDGET'
            when 'MAINSTREAM' then '2. MAINSTREAM'
            when 'PREMIUM' then '3. PREMIUM'
            else NULL
        end as affluence
        ,case cvm.lifestage
            when 'YOUNG SINGLES/COUPLES' then '01. YOUNG SINGLES/COUPLES'
            when 'MIDAGE SINGLES/COUPLES' then '02. MIDAGE SINGLES/COUPLES'
            when 'OLDER SINGLES/COUPLES' then '03. OLDER SINGLES/COUPLES'
            when 'YOUNG FAMILIES' then '04. YOUNG FAMILIES'
            when 'NEW FAMILIES' then '05. NEW FAMILIES'
            when 'OLDER FAMILIES' then '06. OLDER FAMILIES'
            when 'RETIREES' then '07. RETIREES'
            else NULL
        end as lifestage
        ,coalesce(cvm."8wk_spend", 0) as spend_8wk
    from loyalty_modeling.sfx_score_base as bse
    join
        loyalty.customer_value_model as cvm
    on cvm.crn = bse.crn
        and bse.ref_dt - 5 = cvm.pw_end_date::date
) as calc
on calc.crn = upd.crn
;
--^ [4,547,996 rows affected


--==============================================================
/*
    Export
*/
--==============================================================
UNLOAD ('select * from loyalty_modeling.sfx_score_base')
TO 's3://data-preprod-redshift-exports/Joe/sfx_score_base_20200909'
-- TO 's3://data-preprod-redshift-exports/Joe/sfx_score_base_20200705'
CREDENTIALS 'aws_access_key_id=AKIA4KLQCVHRE53KRCQK;aws_secret_access_key=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'
FORMAT PARQUET
ALLOWOVERWRITE
PARALLEL OFF;

-------------------------------------------------
-- QA
-------------------------------------------------
select count(*), min(ref_dt), max(ref_dt)
    ,sum(case when gender is null then 1 else 0 end) as gender
    ,sum(case when macro_segment_curr is null then 1 else 0 end) as macro_segment_curr
    ,sum(case when duration_macro_curr is null then 1 else 0 end) as duration_macro_curr
    ,sum(case when tenure is null then 1 else 0 end) as tenure
    ,sum(case when tenure_band is null then 1 else 0 end) as tenure_band
    ,sum(case when affluence is null then 1 else 0 end) as affluence
    ,sum(case when lifestage is null then 1 else 0 end) as lifestage
from loyalty_modeling.sfx_score_base
;
--^ [10,373,330	2020-08-23	2020-08-23	1,048,100	0	0	0	0	1514507	3730100]

select target_segment, count(*), count(distinct crn)
from loyalty_modeling.sfx_score_base
group by 1
order by 1;

select
    gender
    ,macro_segment_curr
    ,affluence
    ,lifestage
    ,count(*)
from loyalty_modeling.sfx_score_base
group by 1,2,3,4
order by 1,2,3,4;

select
    sfx_pref_dt
    ,sfx_pref_year
    ,wks_to_xmas
    ,count(*)
from loyalty_modeling.sfx_score_base
group by 1,2,3;
--^ [2020-09-06	2020	16	10373330]

select top 18 * from loyalty_modeling.sfx_score_base;
