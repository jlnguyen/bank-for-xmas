# ==============================================================
'''
SCRIPT:
    sfx_config.py

PURPOSE:
    - Save for xmas
    - Config

INPUTS:

OUTPUTS:


DEVELOPMENT:
    Version 1.0 - Joe Nguyen | 10Jul2020
    ---
'''
# ==============================================================
# -------------------------------------------------

# Global
PATH_S3_PRJ = 's3://data-preprod-redshift-exports/Joe'

PATH_GCP_PRJ = 'gs://wx-personal/joe/a03_save_for_xmas'
PATH_GCP_CONFIG = f'{PATH_GCP_PRJ}/config'
PATH_GCP_DATA = f'{PATH_GCP_PRJ}/data'
PATH_GCP_MODELS = f'{PATH_GCP_PRJ}/models'
PATH_GCP_SCORE = f'{PATH_GCP_PRJ}/score'

PATH_LOCAL_PRJ = '/home/jovyan/a02_projects/a09_save_for_xmas'
PATH_LOCAL_CONFIG = f'{PATH_LOCAL_PRJ}/config'
PATH_LOCAL_DATA = f'{PATH_LOCAL_PRJ}/data'
PATH_LOCAL_HELPER = f'{PATH_LOCAL_PRJ}/helper'

PATH_GEN_AKL = '/home/jovyan/a01_repos/gen-akl-feature-config'