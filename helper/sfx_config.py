
RAND_SEED = 123

# Global
PATH_S3_PRJ = 's3://data-preprod-redshift-exports/Joe'

PATH_GCP_PRJ = 'gs://wx-personal/joe/a03_save_for_xmas'
PATH_GCP_CONFIG = f'{PATH_GCP_PRJ}/config'
PATH_GCP_DATA = f'{PATH_GCP_PRJ}/data'
PATH_GCP_MODELS = f'{PATH_GCP_PRJ}/models'
PATH_GCP_SCORE = f'{PATH_GCP_PRJ}/score'

PATH_LOCAL_PRJ = '/home/jovyan/a02_projects/a09_save_for_xmas'
PATH_LOCAL_CONFIG = f'{PATH_LOCAL_PRJ}/config'
PATH_LOCAL_DATA = f'{PATH_LOCAL_PRJ}/data'
PATH_LOCAL_HELPER = f'{PATH_LOCAL_PRJ}/helper'

PATH_GEN_AKL = '/home/jovyan/a01_repos/gen-akl-feature-config'

COLS_INCL = []
COLS_EXCL = [
    'crn',
    'ref_dt',
#     'is_save_for_xmas',
#     'smpl_wgt',
    'sfx_pref_dt',
    'sfx_pref_year',
    'wks_to_xmas',
    'age',
    'age_band',
    'gender',
    'postcode',
    'macro_segment_curr',
    'macro_segment_prev',
    'duration_macro_curr',
    'tenure',
    'tenure_band',
    'preferred_store',
    'affluence',
    'lifestage',
    'spend_8wk',
    'stretch_perc_raw',
]