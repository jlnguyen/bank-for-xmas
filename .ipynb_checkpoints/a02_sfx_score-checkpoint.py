# ==============================================================
'''
SCRIPT:
    a02_sfx_score.py

PURPOSE:
    - Save for xmas
    - Score

INPUTS:

OUTPUTS:


DEVELOPMENT:
    Version 1.0 - Joe Nguyen | 10Jul2020
    ---
'''
# ==============================================================
# -------------------------------------------------


import subprocess
import numpy as np
import pandas as pd
import importlib
import sys
import os

sys.path.insert(0, 'helper')
sys.path.insert(0, '/home/jovyan/a01_repos/wx_tools')

import sfx_config as cfg
import wx_utils as utl
import wx_akl_util as au

# Intialise argo
subprocess.call('~/init.sh', shell=True)

importlib.reload(sys.modules['sfx_config'])
importlib.reload(sys.modules['wx_utils'])
importlib.reload(sys.modules['wx_akl_util'])

# -------------------------------------------------
# Paths
# -------------------------------------------------
cfg_name = 'sfx_config'
cfg_path = f'{cfg.PATH_LOCAL_HELPER}/{cfg_name}.py'

p_scr = au.path_mdl_feature_files(
    dir_mdl='propensity/2020-07-23',
    file_feat_sel='mdl_feat_sel_rdm.xlsx',
    file_mdl_smry='mdl_smry_rdm.json',
    file_feat_spec='mdl_feat_spec_rdm.xlsx',
    is_copy_mdl_files=True,
    cfg_name=cfg_name, cfg_path=cfg_path,
)
p_tbljn = au.path_table_joiner(
    file_base='sfx_score_base_20200719.parquet',
    dir_cmd='scr_20200723',
    file_join_tbls='NA',
    file_join_cols='scr_cols_cmd.txt',
    cfg_name=cfg_name, cfg_path=cfg_path,
)

# -------------------------------------------------
# Get model columns for table joiner
# -------------------------------------------------
cols_scr = au.crt_model_features_list(
    p_scr['path_local_feat_sel'],
    p_scr['path_local_mdl_smry'],
    p_scr['path_local_feat_spec'],
    p_tbljn['path_local_join_cols'],
    p_tbljn['path_gcp_join_cols'],
    upload=True,
    cfg_name=cfg_name, cfg_path=cfg_path,
)

# Copy S3 to GCP
utl.copy_gsutil(p_tbljn["path_s3_base"], p_tbljn["path_gcp_base"])

# Table joiner
utl.call_table_pivoter(
    in_path=p_tbljn['path_gcp_base'],
    out_path=p_tbljn['path_gcp_dir_cmd'],
    file_incl_sources='na',
    file_incl_features=p_tbljn['path_gcp_join_cols'],
    out_file_num='200',
    sparsity_uplimit='1.0',
)
# Instantiating [prd-spark-cmd2-pivoter-v2] with operation [projects/wx-bq-poc/regions/us-east4/operations/6c8966ae-8184-3a2e-ac65-4e4a3f6a46d9].\n'


# ==============================================================
# Distributed Scoring (dist_udf.py)
# ==============================================================
p_dscr = au.path_distributed_scr(
    file_dist_udf='dist_udf_scr.py',
    file_prerun='prerun.sh',
    file_requirements='requirement.txt',
    file_tar='scr_udf.tar.gz',
    file_scr_cfg='scr_config.json',
    file_scr_argo='sfx_scr_argo.yaml',
    cfg_name=cfg_name, cfg_path=cfg_path,
)

# Create tarball file
path_gcp_tar = au.crt_scr_tar_gz(
    p_dscr['path_local_tar'],
    p_dscr['path_local_udf'],
    p_dscr['path_local_prerun'],
    p_dscr['path_local_req'],
    upload=True,
    cfg_name=cfg_name, cfg_path=cfg_path,
)

# Copy score config
path_gcp_scored = au.crt_scr_config(
    '2020-07-23',
    p_tbljn['path_gcp_dir_cmd'],
    path_gcp_tar,
    p_dscr['path_local_udf'],
    p_dscr['path_local_scr_cfg'],
    rm_dir=True,
    upload=True,
    cfg_name=cfg_name, cfg_path=cfg_path,
)

# -------------------------------------------------
# > Create / update argo
# > Run argo
# -------------------------------------------------
subprocess.call(f'argo submit {p_dscr["path_local_argo"]}', shell=True)

!argo list | grep 'joe'
!argo get joe-sfx-score-l5kwf
!kubectl logs joe-sfx-score-l5kwf-788388973 main #> logs/err_scr_20200605_01.txt
!argo delete joe-sfx-score-l5kwf

# Copy scored output to local
path_local_scored = f'{cfg.PATH_LOCAL_DATA}/scored_20200723.parquet'
utl.copy_gsutil(path_gcp_scored, path_local_scored)
