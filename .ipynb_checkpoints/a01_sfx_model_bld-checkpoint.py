# ==============================================================
'''
SCRIPT:
    a01_sfx_model_bld.py

PURPOSE:
    - Save for xmas
    - Model build

INPUTS:

OUTPUTS:


DEVELOPMENT:
    Version 1.0 - Joe Nguyen | 07Jul2020
    ---
'''
# ==============================================================
# -------------------------------------------------


import subprocess
import numpy as np
import pandas as pd
import importlib
import sys
import os

sys.path.insert(0, 'helper')
sys.path.insert(0, '/home/jovyan/a01_repos/wx_tools')

import sfx_config as cfg
import wx_utils as utl
import wx_akl_util as au

# Intialise argo
subprocess.call('~/init.sh', shell=True)

importlib.reload(sys.modules['wx_utils'])
importlib.reload(sys.modules['wx_akl_util'])

# -------------------------------------------------
# Paths
# -------------------------------------------------
cfg_name = 'sfx_config'
cfg_path = f'{cfg.PATH_LOCAL_HELPER}/{cfg_name}.py'

p_tbljn = au.path_table_joiner(
    file_base='sfx_model_data.parquet',
    dir_cmd='mdl_20200723',
    file_join_tbls='sfx_cmd_sources.txt',
    file_join_cols='NA',
    cfg_name=cfg_name, cfg_path=cfg_path,
)
p_mdl = au.path_akl_config(
    file_cfg_xlsx='cfg_sfx_20200723.xlsx',
    file_cfg_akl='sfx_akl.yaml',
    file_argo='sfx_argo.yaml',
    cfg_name=cfg_name, cfg_path=cfg_path,
)

# -------------------------------------------------
# Copy files
# -------------------------------------------------
# Copy S3 to GCS
utl.copy_gsutil(p_tbljn["path_s3_base"], p_tbljn["path_gcp_base"])
utl.copy_gsutil(p_tbljn["path_local_join_tbls"], p_tbljn["path_gcp_join_tbls"])

# -------------------------------------------------
# Table joiner
# -------------------------------------------------
utl.call_table_pivoter(
    in_path=p_tbljn["path_gcp_base"],
    out_path=p_tbljn["path_gcp_dir_cmd"],
    file_incl_sources=p_tbljn["path_gcp_join_tbls"],
    out_file_num='50',
    sparsity_uplimit='0.99',
)
# Instantiating [prd-spark-cmd2-pivoter-v2] with operation [projects/wx-bq-poc/regions/us-east4/operations/8489eedf-6176-35c7-b058-87646a3cffee].\n'


# ==============================================================
# Model Build
# ==============================================================
# -------------------------------------------------
# 0. Create / update excel config
# -------------------------------------------------
# Get one w_cmd partition (from GCS location)
file_cmd_part = 'part-00000-61542ca5-d1bd-428d-b67f-5381eba96e4e-c000.snappy.parquet'
file_local_cmd_part = 'sfx_w_cmd_20200708.parquet'
path_gcp_cmd_part = f'{path_gcp_dir_cmd}/{file_cmd_part}'
path_local_cmd_part = f'{cfg.PATH_GEN_AKL}/inbound/{file_local_cmd_part}'
# utl.copy_gsutil(path_gcp_cmd_part, path_local_cmd_part)

# > Run gen-akl
# > Copy excel config to project config
file_cfg_xlsx = 'cfg_sfx_20200713.xlsx'
path_akl_outbound = f'{cfg.PATH_GEN_AKL}/outbound/{file_cfg_xlsx}'

subprocess.check_call(
    f'cp {path_akl_outbound} {p_mdl["path_local_cfg_xlsx"]}',
    shell=True, stderr=subprocess.STDOUT)

# > Manually inspect and alter rdm and spd xlsx configs

# -------------------------------------------------
# 1. Create / update config
# -------------------------------------------------
# > Manually create AKL config and argo
# 1a Copy configs
utl.copy_gsutil(p_mdl["path_local_cfg_xlsx"], cfg.PATH_GCP_CONFIG)
utl.copy_gsutil(p_mdl["path_local_cfg_akl"], cfg.PATH_GCP_CONFIG)

# -------------------------------------------------
# 2. Create / update argo
# -------------------------------------------------
# 2a. Run argo
subprocess.call(f'argo submit {p_mdl["path_local_argo"]}', shell=True)

!argo list | grep 'joe'

!argo get joe-sfx-6pnz5
!kubectl logs joe-sfx-lwlr8-2778207629 main
!kubectl describe pod joe-sfx-nowks-889kt-1979221223

!argo delete joe-sfx-lwlr8
!argo delete joe-sfx-score-97qqz
!argo delete joe-sfx-score-6b7xq

# GCP logs text
path = f'{cfg.PATH_GCP_MODELS}/propensity/2020-07-27/log/feature_selection.txt'
_ = utl.print_logs_gcs(path)

# -------------------------------------------------
# Downsample redeem roc.json
# -------------------------------------------------
path_gcp_roc = f'{cfg.PATH_GCP_MODELS}/propensity/2020-07-23/diagnosis/roc.json'
path_local_roc = f'{cfg.PATH_LOCAL_DATA}/roc.json'
path_local_roc_smpl = f'{cfg.PATH_LOCAL_DATA}/roc_smpl.json'

roc_smpl = utl.akl_roc_downsample(
    path_gcp_roc, path_local_roc, path_local_roc_smpl,
    download=True, upload=True,)

# Diagnosis paths
https://diagnostics-0-5-dot-wx-bq-poc.ts.r.appspot.com/path/wx-personal/joe/a03_save_for_xmas/models/propensity/2020-07-13/diagnosis/
https://diagnostics-0-5-dot-wx-bq-poc.ts.r.appspot.com/path/wx-personal/joe/a03_save_for_xmas/models/propensity/2020-07-22/diagnosis/
https://diagnostics-0-5-dot-wx-bq-poc.ts.r.appspot.com/path/wx-personal/joe/a03_save_for_xmas/models/propensity/2020-07-23/diagnosis/

# ==============================================================
# Check W_CMD data
# ==============================================================
file_cmd_part = 'part-00000-8f315518-993f-47c7-8eda-6a1b7eb5b2b2-c000.snappy.parquet'
file_local_cmd_part = 'sfx_w_cmd_20200708_test.parquet'
path_gcp_cmd_part = f'{path_gcp_dir_cmd}/{file_cmd_part}'
path_local_cmd_part = f'{cfg.PATH_LOCAL_DATA}/{file_local_cmd_part}'
utl.copy_gsutil(path_gcp_cmd_part, path_local_cmd_part)

data = pd.read_parquet(path_local_cmd_part)
data.dtypes