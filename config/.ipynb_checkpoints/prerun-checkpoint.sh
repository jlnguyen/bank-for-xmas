## AKL 0.5
pip3 install --no-cache-dir -r requirement.txt

# clone auto_ml repository
rm -rf auto_ml && gcloud source repos clone bitbucket_wx_5Frds_auto_5Fml auto_ml

# fetch master
cd auto_ml
git fetch --tags --all
git checkout tags/0.5
cd ..

# move code
rm -rf lgbmfit && rm -rf helper
mv auto_ml/lgbmfit .
mv auto_ml/helper .

# download model objects
gsutil -mq cp gs://wx-personal/joe/a03_save_for_xmas/models/propensity/2020-07-23/model_selector/model_object.pickle ./model_propensity.pickle
# gsutil -mq cp gs://wx-personal/joe/a03_save_for_xmas/models/propensity/2020-07-13/model_selector/model_object.pickle ./model_propensity.pickle

# # Switch "from .utils import *" from 2nd line to top
# #^ to handle: "OSError: dlopen: cannot load any more object with static TLS"
# sed -i '2d' lgbmfit/fit.py
# sed -i '1 i\from .utils import *' lgbmfit/fit.py
