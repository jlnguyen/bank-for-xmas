# ==============================================================
# AKL V0.5
# ==============================================================
# Save for Xmas | Propensity | bin-cls: is_save_for_xmas | 1.1M rows
global:
  modeller: Joe Nguyen
  google_project: wx-bq-poc
  google_bucket: wx-personal
  google_subdir: joe/a03_save_for_xmas/models
  model_name: propensity
  run_date: '2020-07-27'
  objective: binary
  target: is_save_for_xmas
  sample_weight: smpl_wgt           # User defined weight column, otherwise leave as sample_weight
  ids:                              # Variables to keep in output, but not used for training
  - crn
  - ref_dt
  metric:
      # Metrics that are used during training and available metrics include:
      # For regression: l1, l2, mape, map, rmse, quantile, mse, kullback_leibler
      # For Classification: auc, cross_entropy,  binary_logloss,  binary_error, kullback_leibler
  - auc
  - binary_logloss
input:
  data_addr: wx-personal/joe/a03_save_for_xmas/data/w_cmd/mdl_20200723/*
    # For single file, use the exact file path:
    # e.g.: wx-auto-ml/test_branch/sample/rdm_data_reduce.parquet
    # For partitions, use the folder path followed by *:
    # e.g.: wx-auto-ml/test_branch/sample/*
  config_addr: wx-personal/joe/a03_save_for_xmas/config/cfg_sfx_20200722.xlsx
    # For excel files on gcs:
    # e.g.: wx-auto-ml/test_branch/sample/RdmLabel.xlsx
    # For google sheet formats on google drive, it should be the url of the file:
    # e.g.: https://docs.google.com/spreadsheets/d/1ttLSimKGRKu_nIXUqBVVkis9NFzfhHfX823AuVU2Q04/edit#gid=87460757
# ------------------------------------------------------------------------------
preprocessor:
  n_cpu : 30
      # Number of CPU that is used for preproceessing and generating config
      # It is an optional feature and the default number is maximun cpu - 5
      # If your data is larger than 5 million, you should use a smaller number cpu
  chunk_size : 30
      # Number of features in chunk and default number is 30
      # If your data is larger than 5 million, you should use a smaller number features
  sparsity: 0.99
      # Sparsity threshold for filter out sparse data, ranging from 0 to 1 
      # It is an optional feature and default number is 1 which means turning off the filter
      # In that is 1, it only filters the features with only none or features with one unique value
  target_missing_handle: keep
      # Two options: keep/remove
      # keep: records with missing target are filled with 0
      # remove: remove the records with missing target
  params:
    sampling:
      sample_on: train # train or full
      # Option 1: uniform sampling by target value
      # value: 0, 1
      # rate: 1, 1
      # Option 2: uniform sampling for the whole data
      rate: 1
      random_state: 123 # Random seed of generating test set
    train_test_holdout:
      # Two options: by_time/by_proportion
      # by_proportion: only generate train/test set
      # by_time: generate holdout set with user define time threshold
      #          The final output will have train/test/holdout set
      split_type: by_time
      by_proportion: 80 # Train size, 0-100 or 0.0 - 1.0
      by_time:
        format: '%Y-%m-%d' # Time format for holdout
        timeVar: ref_dt # Define the variable for time variable
        inTimeEndDate: '2020-06-01' # Define the end time of training data
        inTimeTrainTestSplit: 80 # Training size, same with 'by_proportion'
# ------------------------------------------------------------------------------
feature_selection:
  skip: false
  # After V0.5, we add the skip key to skip feature selection.
  iteration: 0, 2, 4
  # After V0.5, we support multi-level feature selection.
  # If you set it to '0,2,4',
  # the first and second iterations are using the first set of parameter
  # the 3nd and 4th iterations are using the second set of parameter
  # after the 4th iteration, all of iterations are using the third set of parameter
  hard_sampling: 0.4, 0.6, 0.8
  # After V0.5, we support multi-level hard_sampling in feature selection.
  # You should set the same number of and order with iteration.
  # In this case
  # the first two iterations will use 20% of training data for feature selection
  # the 3nd and 4th iterations will use 40% of training data for feature selection
  # after the 4th iteration, all of iterations will use 60% of training data for feature selection
  training_params:
    label_encode: true # Enable label encode or not: true/ false
    early_stopping_rounds: 50
    random_state: 777
    model_spec:
      # After V0.5, we support multi-level hard_sampling in feature selection.
      # You should set the same number of and order with iteration
      # Or just 1 parameter which means that all iteration will use the same set of parameter

      # AKL supports more than these parameters
      # Detailed information could be found at
      # https://lightgbm.readthedocs.io/en/latest/Parameters.html
      n_estimators: 1500, 2000, 2500
      max_depth: 4, 5, 6 #8
      num_leaves: 12, 20, 40 # set to < 2^{max_depth} for better accuracy
      min_data_in_leaf: 200
      learning_rate: 0.1, 0.075, 0.04
      random_state: 123
      reg_alpha: 0.2
      reg_lambda: 0.2
      feature_fraction: 0.8
      bagging_freq: 1
      bagging_fraction: 0.9
  CV: 3               # Number of cross validation folds, cannot be set to 1 # 4
  # feature importance is relative to the feature with maximum importance
  min_imp: .001       # minimum importance required for selection # 0.00001, .0005, .0001
  inc_imp: .002       # size of increment in cut-off threshold    # 0.00001, .001, .0005
  min_var: 80         # stopping condition 1: stop after cutting below this threshold
  max_step: 20        # stopping condition 2: stop after reaching number of iterations
  min_cut: 0.05
  max_cut: 0.9
  # After V0.5, we introduce min_cut and max_cut paremeters which are option.
  # These parameters are controling the presentage of features being dropped at each iteration.
  # min_cut's default value is 0 and max_cut's default value is 1
  # min_cut should be smaller than max_cut, and both are ranging from 0 to 1
  # min_cut represents the min presentage of feature being dropped at each iteration.
  # max_cut represents the max presentage of feature being dropped at each iteration.
  output_iteration: 8
  # After V0.5, we support an new function that user can select the iteration they want
  # If you put best, we will select the best iteration, according to the validation score
  # If you put integer like 5, we will select the features from the 5th iteration
  # If you put -1, we will just output the features selected from the last iteration
  forceInputFeature:
  - 'f0_macro_segment_curr'
  - 'f0_affluence'
  - 'f0_lifestage'
  - 'f0_age_band'
  - 'f0_age'
  - 'f0_tenure_band'
  - 'f0_sfx_pref_year'
  - 'f0_wks_to_xmas'
  # - ''
      # White list of features that be forced into fitting
      # Example:
      # - 'f0_campaign_type'
# ------------------------------------------------------------------------------
fit:
  training_params:
    label_encode: true
    early_stopping_rounds: 50
    nfolds: 5                 # Number of cross validation folds, can be set to 1
    train_rate: 70            # If nfolds=1, 70% of data will be used for training
    random_state: 777
  algo_max_tune:
    # Number of random grid search iterations for each boosting type
    gbdt: 12
    # dart: 4 (optional)
    # AKL supports more than these parameters
    # Detailed information could be found at
    # https://lightgbm.readthedocs.io/en/latest/Parameters.html
  gbdt_hyper_params:
    n_estimators: 1800, 2200, 2500
    max_depth: 4, 5, 6, 8
    num_leaves: 12, 20, 35, 50
    min_data_in_leaf: 100, 200, 500, 1000
    learning_rate: 0.04, 0.03, 0.015, 0.01
    reg_alpha: 0.1, 0.2, 0.5, 0.8, 1, 3
    reg_lambda: 0.1, 0.2, 0.5, 0.8, 1, 3
#     bagging_freq: 0,1,5,10 # deactivate due to already downsampled (with inverse weights)
#     pos_bagging_fraction: 1
#     neg_bagging_fraction: 0.7,0.8,0.9
    random_state: 777
model_selector:
  user_specified_model: ''
      # Default to '', AKL selects iteration with best validation performance
      # User can overwrite with iteration number
      # Use diagnostic dashboard to see performance between iterations
      # Option 1: select the best validation performance
      # user_specified_model: ''
      # Option 2: select user define model like the first one
      # user_specified_model: 1
diagnosis:
  diag_groups: 15
      # Optional parameter and the default one is 10
      # Set number of groups for each feature to calculate the mean of prediction and actual
      # If your category feature unique value is larger than this, 
      # We will set to the number of max category feature unique value
  optional_features:
  - 'f0_macro_segment_curr'
  - 'f0_affluence'
  - 'f0_lifestage'
  - 'f0_age_band'
  - 'f0_age'
  - 'f0_tenure_band'
  - 'f0_sfx_pref_year'
  - 'f0_wks_to_xmas'
      # Additional features for goodness of fit plots
  shap_para:
    cutoff_method: quantile # quantile or absolute
    min: 0.01
    max: 0.99
  metrics:
  - f1_score
  - recall_score
  - precision_score
  - binary_error
  - auc
  - accuracy_score
  # After V0.4c, we support an new function that user can select metrics to be calculated in diag
  # For regression we have:
  #  explained_variance_score,mean_absolute_error,mean_squared_error,
  #  mean_absolute_error,mean_squared_log_error,median_absolute_error
  # For classification we have:
  #  accuracy_score,auc,average_precision_score,
  #  f1_score,log_loss,binary_logloss,
  #  precision_score,r2_score, r2,recall_score,zero_one_loss,binary_error