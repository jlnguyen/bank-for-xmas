# Save for Xmas: distributed scoring

# ** Retain import order to address issue:
# "OSError: dlopen: cannot load any more object with static TLS" **
import lightgbm as lgb
import sys
import pyarrow.parquet as pq
import pandas as pd
import os
import pickle
import numpy as np
from auto_ml.preprocessor.numeric import PreprocessorNum
from auto_ml.preprocessor.categorical import PreprocessorCat


def safe_make_folder(i):
    '''Makes a folder if not present'''
    try:
        os.mkdir(i)
    except:
        pass

    return None


def load_model(path):
    model_obj = pickle.load(open(path, 'rb'))
    return model_obj


def get_feature_list(input_path, model_feature_list, model_obj):
    ''' Get numeric and categorical features
    '''
    # get schema
    schema = pq.ParquetFile(input_path).metadata.schema
    feature_list = [item.name for item in schema]

    # get spec for both bum and cat
    spec_num = model_obj.config['spec_num']
    spec_cat = model_obj.config['spec_cat']

    cond = (
        (spec_num.input != 'ignore') &
        (spec_num.new_feature.isin(model_feature_list))
    )
    spec_num_features = spec_num[cond].feature

    cond = (
        (spec_cat.input != 'ignore') &
        (spec_cat.new_feature.isin(model_feature_list))
    )
    spec_cat_features = spec_cat[cond].feature
    num_list = [item for item in spec_num_features if item in feature_list]
    cat_list = [item for item in spec_cat_features if item in feature_list]

    return num_list, cat_list


def read_num_df(input_path, num_list):
    '''read in num df
    '''
    num_df_list = []
    _table = pq.read_table(input_path, columns=num_list)
    _list = [_table.column(x).cast('float').to_pandas() for x in num_list]
    _df = pd.concat(_list, axis=1)
    _df.columns = num_list

    num_df_list.append(_df)
    num_df = pd.concat(num_df_list, axis=1)

    return num_df


def read_cat_df(input_path, cat_list):
    '''read in cat df
    '''
    cat_df_list = []
    _df = pq.read_table(input_path, columns=cat_list)
    _df = _df.to_pandas().astype(str)
    cat_df_list.append(_df)
    cat_df = pd.concat(cat_df_list, axis=1)

    return cat_df


# ==============================================================
# Load and prepare data based on model
# ==============================================================
def load_prep_data(input_path, loaded_model, ids):

    # get spec for both bum and cat
    spec_num = loaded_model.config['spec_num']
    spec_cat = loaded_model.config['spec_cat']

    # get list of model features
    # .values # lgbm==2.3.1 changed API from series to list
    model_feature_list = loaded_model.columns
    model_feature_list = (
        pd.DataFrame(model_feature_list, dtype=str)[0]
        .apply(lambda x: x.split('||')[0])
    )
    num_list, cat_list = get_feature_list(
        input_path, model_feature_list, loaded_model)

    # separate in numerical and categorical
    num_df = pd.read_parquet(input_path, columns=num_list)
    cat_df = pd.read_parquet(input_path, columns=cat_list)

    # Process Numeric Attribute
    if num_df.shape[0] != 0:
        pipeline_num = PreprocessorNum(spec_num, update_spec=False)
        processed_num_data = pipeline_num.transform(num_df)
    else:
        processed_num_data = pd.DataFrame()
    del num_df

    # Process Categorical attribute
    if cat_df.shape[0] != 0:
        pipeline_cat = PreprocessorCat(spec_cat, update_spec=False)
        processed_cat_data = pipeline_cat.transform(cat_df)
    else:
        processed_cat_data = pd.DataFrame()
#     print(cat_df['campaign_type'].value_counts().sort_index())
#     print(processed_cat_data['f0_campaign_type'].value_counts().sort_index())
    del cat_df

    output_data = pd.concat([
        ids,
        processed_num_data,
        processed_cat_data], axis=1)

    print(f"scored data shape: {output_data.shape}")

    output_data['score'] = loaded_model.predict(output_data)

    # Subset columns
    cols = ['crn', 'score']
    return output_data[cols].drop_duplicates()


def main():
    pd.options.mode.chained_assignment = None
    
    # CMD data file
    input_path = sys.argv[1]
#     input_path = './scr_part.parquet'

    # Information columns
    cols = [
        'ref_dt',
        'crn',
        'macro_segment_curr',
        'affluence',
        'lifestage',
        'spend_8wk',
        'gender',
        'age',
        'age_band',
        'tenure',
        'tenure_band',
        'wks_to_xmas',
    ]

    data = pd.read_parquet(input_path, columns=cols)
    data['crn'] = data['crn'].values.astype('str')
    
    mdl = load_model("./model_propensity.pickle")
    
    tmp = load_prep_data(input_path, mdl, data['crn'])
    scored = data.merge(tmp, on=['crn'], how='left')
    
    print(f'post-score data shape: {scored.shape}')

    safe_make_folder('output')
    scored.to_parquet('./output/processed.parquet')

    return None


if __name__ == '__main__':
    main()
